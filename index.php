<?php
require_once "helper.php";

if (isset($_GET['alertType'])) {
	echo $twig->render('templates/index.twig', array('alertType' => $_GET['alertType'], 'alertText' => $_GET['alertText']));
} else {
	echo $twig->render('templates/index.twig');
}