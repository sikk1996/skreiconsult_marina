<?php
/**
 * Created by PhpStorm.
 * User: Peter
 * Date: 18.02.2019
 * Time: 22:39
 */
require_once "helper.php";
$alertType = "";
$alertText = "";

if (isset($_SESSION['user_role']) && $_SESSION['user_role'] >= 3) {

    $applications = Application_db::getAllApplications();
    $docks = Dock_db::getDocksWithAvailableAndAccessibleDockings();
    $dockings = Docking_db::getAllDockings();


    if (isset($_POST['acceptFramleie']) && isset($_POST['framleieAppl'])) {
        //Opprett framleie
        Docking_db::createNewFramleie($_POST['framleieAppl'],$_POST['acceptFramleie']);

        //søknad om BÅTPLASS godkjent
        Application_db::setStatus($_POST['framleieAppl'],2);

        //søknad om FRAMLEIE godkjent
	    Application_db::setStatus($_POST['acceptFramleie'],4);

        //Send epost til FRAMLEIE søker
        sendMail($_POST['userEmail'], "Din framleiesøknad er godkjent", "Hei! <br>Din søknad om framleie hos Skrei Marina har nå blitt akseptert. <br>Logg inn på https://109.189.39.179:63636/login.php for å finne ut mer.");

        //Send email til BÅTPLASS søker TODO: Sjekk om det funker :p
        $application = Application_db::getApplicationByID($_POST['framleieAppl']);
        $boat = Boat_db::getBoatByBoatId($application->getBoatID());
        $user = User_db::getUserByUserId($boat->getUserId());
        sendMail($user->getEmail(), "Din søknad om båtplass er godkjent", "Hei ".$user->getFirstname()."! <br>Din søknad om båtplass, for båten '".$boat->getBoatName()."' hos Skrei Marina har nå blitt akseptert. <br>Logg inn på https://109.189.39.179:63636/login.php for å finne ut mer.");

        //Refresh siden
        header('Location: applications.php');

    } elseif (isset($_POST['rejectFramleie'])){
        Application_db::deleteApplication($_POST['rejectFramleie']);
        sendMail($_POST['userEmail'], "Din framleiesøknad hos Skrei Marina er avslått", "Hei! <br>Din søknad om framleie hos Skrei Marina har dessverre blitt avslått. <br>Logg inn på https://109.189.39.179:63636/login.php for å sende ny søknad.");
        header('Location: applications.php');

    }

    if (isset($_POST['accept']) && isset($_POST['docking'])) {

    	//Delete existing framleie
    	$application = Application_db::getApplicationByID($_POST['accept']);
    	if ($application->getStatus()==2){
		    try {
			    $framleie = Docking_db::getFramleieByBoatID($application->getBoatID());
			    $framleieAppl= Application_db::getApplicationByDockingID($framleie->getDockingID());
			    Application_db::setStatus($framleieAppl->getApplicationID(),3);
			    Docking_db::deleteFramleie($framleie->getDockingID());
		    }catch(Exception $e){

		    }
	    }

        if (DockingAssigner::assignDocking($_POST['accept'], $_POST['docking'])) {
            $alertType = "alert-success";
            $alertText = "Assigned Boat to Docking";
            Application_db::setStatus($_POST['accept'], 1);
            sendMail($_POST['userEmail'], "Du har blitt tildelt en båtplass hos Skrei Marina", "Hei! <br>Din søknad om båtplass hos Skrei Marina har nå blitt akseptert. <br>Logg inn på https://109.189.39.179:63636/login.php for å finne ut mer.");
            header('Location: applications.php');
        } else {
            $alertType = "alert-danger";
            $alertText = "Assignment failed";
        }

    } elseif (isset($_POST['reject'])) {
	    //Delete existing framleie
	    $application = Application_db::getApplicationByID($_POST['accept']);
	    if ($application->getStatus()==2){
		    try {
			    $framleie = Docking_db::getFramleieByBoatID($application->getBoatID());
			    $framleieAppl= Application_db::getApplicationByDockingID($framleie->getDockingID());
			    Application_db::setStatus($framleieAppl->getApplicationID(),3);
			    Docking_db::deleteFramleie($framleie->getDockingID());
		    }catch(Exception $e){

		    }
	    }
        Application_db::deleteApplication($_POST['reject']);
        sendMail($_POST['userEmail'], "Din søknad hos Skrei Marina avslått", "Hei! <br>Din søknad om båtplass hos Skrei Marina har dessverre blitt avslått. <br>Logg inn på https://109.189.39.179:63636/login.php for å sende ny søknad.");
        header('Location: applications.php');
    }

    echo $twig->render('templates/applications.twig', array(
        'applications' => $applications,
        'docks' => $docks,
        'alertType' => $alertType,
        'alertText' => $alertText,
        'dockings' => $dockings,
        'users' => User_db::getAllUsers(),
        'boats' => Boat_db::getAllBoats()
    ));

} else {
    header('Location: index.php');
    echo "error";

}