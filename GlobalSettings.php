<?php
/**
 * Created by PhpStorm.
 * User: Håkon Finstad
 * Date: 4/9/2019
 * Time: 12:55 PM
 */

require_once "helper.php";
$feedbackValidated = true;
$feedbackTitle = "";
$feedbackText = "";


function goToIndexPage()
{
	header('Location: index.php');
}
function refreshPage()
{
    header('Location: GlobalSettings.php');

}
$marinaSettings = Settings_db::getSettings();

if(isset($_POST['changeMarinaName']))
{
    $marinaName = $_POST['marinaName'];
	$feedbackValidated = validateInputs("Feilmelding", "Ugyldig tegn i havnenavnet", $marinaName, 'string');
	if($feedbackValidated){
		Settings_db::setMarinaName($marinaName);
	    refreshPage();
	}
}
if(isset($_POST['changeMemberPayment']))
{
    $memberPayment = $_POST['memberPayment'];
    $feedbackValidated = validateInputs("Feilmelding", "Ugyldig tegn i medlems beløpet", $memberPayment, 'integer');
    if($feedbackValidated){
        $memberPayment = $_POST['memberPayment'];
        Settings_db::setMemberAmount($memberPayment);
        refreshPage();
    }
}
if(isset($_POST['changeDockingPayment']))
{
    $dockingPayment = $_POST['dockingPayment'];
    $feedbackValidated = validateInputs("Feilmelding", "Ugyldig tegn i båtplass beløpet", $dockingPayment, 'integer');
    if($feedbackValidated){
        $dockingPayment = $_POST['dockingPayment'];
        Settings_db::setDockingPaymentAmount($dockingPayment);
        refreshPage();
    }
}

if (isset($_SESSION['user_role'])) {
	if ($_SESSION['user_role'] >= 3) {
		echo $twig->render('templates/settings.twig', array('settings' => $marinaSettings ,'feedbackValidated' => $feedbackValidated, 'feedbackTitle' => $feedbackTitle, 'feedbackText' => $feedbackText));
	}else goToIndexPage();
}else goToIndexPage();


function validateInputs($title, $text, $input, $type)
{
	global $feedbackText;
	global $feedbackTitle;
	global $feedbackValidated;

	if($type == 'string')
    {
        if(!helperFunctions::validateString(array($input)))
        {
            $feedbackTitle = $title;
            $feedbackText = $text;
            return false;
        }
    }else if($type == 'integer')
    {
        if(!helperFunctions::validateInteger($input))
        {
            $feedbackTitle = $title;
            $feedbackText = $text;
            return false;
        }
    }
	return true;

}