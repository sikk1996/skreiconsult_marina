<?php

require_once "helper.php";
$loginFeedback = "";

if (isset($_SESSION['user_role'])) {
	if ($_SESSION['user_role'] > 0) {
		header('Location: profile.php');
	}
}
$email = null;
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	if (isset($_POST['logInBtn'])) {
		$user = User_db::getUserByEmail($_POST['emailInput']);

		if(isset($_POST['emailInput'])){
		$email= $_POST{'emailInput'};
		}
		if ($user instanceof User) {
			if (helperFunctions::comparePasswordHash($user->getpasswordHash(), $_POST['passwordInput'])) {
				//you can log inn
				$_SESSION['user_id'] = $user->getUserID();
				$_SESSION['user_role'] = $user->getRole();
				$_SESSION['user_name'] = $user->getFirstname() . " " . $user->getLastname();
				header('Location: profile.php');
			} else {
				$loginFeedback = "Feil passord";
			}
		} else {
			$loginFeedback = "Brukeren eksisterer ikke";
		}
	}
	if (isset($_POST['registerBtn'])) {
		header('Location: registerUser.php');
	}
}
echo $twig->render('templates/login.twig', array('loginFeedback' => $loginFeedback, 'email'=> $email));