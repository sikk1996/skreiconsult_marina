<?php
require_once "helper.php";
$feedbackValidated = true;
$feedbackTitle = "";
$feedbackText = "";

if (isset($_SESSION['user_role']) && $_SESSION['user_role'] < 3) {
	header('Location: profile.php"');
} else {
	if (isset($_POST['confirm'])) {
		if (!isset($_SESSION['user_role'])) {
			if (!isset($_POST['agreeToSCheck'])) {
				$feedbackTitle = "Feilmelding";
				$feedbackText = "Sjekkboks må være huket av";
				$feedbackValidated = false;
			}
			if (!helperFunctions::checkIfPasswordMatches($_POST['pwdInput1'], $_POST['pwdInput2'])) {
				$feedbackTitle = "Feilmelding";
				$feedbackText = "Passordene må være like";
				$feedbackValidated = false;
			}
			if (!helperFunctions::legalPassword($_POST['pwdInput1']) && !helperFunctions::legalPassword($_POST['pwdInput2'])) {
				$feedbackTitle = "Feilmelding";
				$feedbackText = "Passordet må ha minst 6 tegn";
				$feedbackValidated = false;
			}
		}
		if (!helperFunctions::validateEmail($_POST['emailInput'])) {
			$feedbackTitle = "Feilmelding";
			$feedbackText = "Epostadresse ikke gyldig";
			$feedbackValidated = false;
		}
		if (!helperFunctions::validateString(array($_POST['firstNameInput'], $_POST['lastNameInput']))) {
			$feedbackTitle = "Feilmelding";
			$feedbackText = "Navn inneholder ugyldige tegn";
			$feedbackValidated = false;
		}
		if (!helperFunctions::validateInteger($_POST['phoneInputOne']) && !helperFunctions::validatePhoneNumber($_POST['phoneInputOne'])) {
			$feedbackTitle = "Feilmelding";
			$feedbackText = "Mobilnummer må ha minst 8 tegn";
			$feedbackValidated = false;
		}
		if (!empty($_POST['phoneInputTwo'])) {
			if (!helperFunctions::validateInteger($_POST['phoneInputTwo'] && !helperFunctions::validatePhoneNumber($_POST['phoneInputTwo']))) {
				$feedbackTitle = "Feilmelding";
				$feedbackText = "Telefonnummer må ha minst 8 tegn";
				$feedbackValidated = false;
			}
		}
		if (!helperFunctions::validateString(array($_POST['postaddressInput']))) {
			$feedbackTitle = "Feilmelding";
			$feedbackText = "Adresse innholder ugyldige tegn";
			$feedbackValidated = false;
		}
		if (!helperFunctions::validateInteger($_POST['postcodeInput'])) {
			$feedbackTitle = "Feilmelding";
			$feedbackText = "Postkode inneholder ugyldige tegn";
			$feedbackValidated = false;
		}

		if ($feedbackValidated) {
			addNewUser();
		}
	}
	echo $twig->render('templates/registerUser.twig', array('feedbackValidated' => $feedbackValidated, 'feedbackTitle' => $feedbackTitle, 'feedbackText' => $feedbackText));
}

function addNewUser() {
	$rawPassword = "";
	$firstname = $_POST['firstNameInput'];
	$lastname = $_POST['lastNameInput'];
	$email = $_POST['emailInput'];
	$phone1 = is_numeric($_POST['phoneInputOne']) ? (int)$_POST['phoneInputOne'] : null;
	$_POST['phoneInputOne'];
	$phone2 = $_POST['phoneInputTwo'];
	if ($phone2 == "")
		$phone2 = null;
	$postCode = is_numeric($_POST['postcodeInput']) ? (int)$_POST['postcodeInput'] : null;
	$postAddress = $_POST['postaddressInput'];
	if (isset($_SESSION['user_role']) && $_SESSION['user_role'] >= 3) {
		$rawPassword = helperFunctions::randomPassword(15);
	} else {
		$rawPassword = $_POST['pwdInput1'];
	}

	$password = helperFunctions::encryptPassword($rawPassword);
	User_db::newUser($email, $password, $firstname, $lastname, $phone1, $phone2, $postAddress, $postCode);

	$userId = User_db::getUserByEmail($email)->getUserID();
	$key = User_db::getVerificationKeyByUserId($userId);
	$verifyUrl = "https://109.189.39.179:63636/verify.php?id=" . $userId . "&keyValue=" . $key;

	if ($_SESSION['user_role'] == 3) {
		sendMail($email, "Verifisering av bruker hos SkreiMarina", "Hei, det er opprettet en bruker for deg hos SkreiMarina. <br>Benytt dette passordet ved første innlogging: " . $rawPassword . " <br> Vennligst verifiser din brukerkonto hos SkreiMarina ved å trykke på linken: <br>" . $verifyUrl);
		header('Location: users.php?alertType=alert-info&alertText=Bruker opprettet: ' . $email);
	} else {
		sendMail($email, "Verifisering av bruker hos SkreiMarina", "Hei, verfiser din brukerkonto hos SkreiMarina ved å følg linken: <br>" . $verifyUrl);
		header('Location: profile.php');
	}
}