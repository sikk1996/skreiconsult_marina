<?php
/**
 * Created by PhpStorm.
 * User: berge
 * Date: 25.03.2019
 * Time: 13.35
 */

class VoluntaryWork {
    private $workID;
    private $userID;
    private $hoursRegistered;
    private $description;
    private $date;

    /**
     * @return mixed
     */
    public function getWorkID()
    {
        return $this->workID;
    }

    /**
     * @return mixed
     */
    public function getUserID()
    {
        return $this->userID;
    }

    /**
     * @return mixed
     */
    public function getHoursRegistered()
    {
        return $this->hoursRegistered;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }
}