<?php


class Framleie
{
private $framleieID;
private $dockingID;
private $boat;
private $framleieUserID;

    /**
     * @return mixed
     */
    public function getFramleieID()
    {
        return $this->framleieID;
    }

    /**
     * @return mixed
     */
    public function getDockingID()
    {
        return $this->dockingID;
    }

    /**
     * @return mixed
     */
    public function getBoat()
    {
        return $this->boat;
    }

    /**
     * @return mixed
     */
    public function getFramleieUserID()
    {
        return $this->framleieUserID;
    }

}