<?php
/**
 * Created by PhpStorm.
 * User.class: Peter
 * Date: 18.02.2019
 * Time: 22:28
 */

class Docking
{
    private $dockingID;
    private $dock;
    private $boat;
    private $ownerID;
	private $subleaseToUser;
	private $status;

    /**
     * @return mixed
     */
    public function getSubleaseToUser()
    {
        return $this->subleaseToUser;
    }


    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }


    /**
     * @return mixed
     */
    public function getDockingID()
    {
        return $this->dockingID;
    }

    /**
     * @return mixed
     */
    public function getDock()
    {
        return $this->dock;
    }

    /**
     * @return mixed
     */
    public function getBoat()
    {
        return $this->boat;
    }/**
 * @return mixed
 */public function getOwnerID()
{
	return $this->ownerID;
}





}