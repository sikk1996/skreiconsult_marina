<?php
/**
 * Created by PhpStorm.
 * User.class: Peter
 * Date: 18.02.2019
 * Time: 22:28
 */

class Dock
{
    private $dockID;
    private $nickname;
    private $dockBoss;
    private $status;
    private $marina;

    public function getDockID()
    {
        return $this->dockID;
    }

    /**
     * @return mixed
     */
    public function getNickname()
    {
        return $this->nickname;
    }

    /**
     * @return mixed
     */
    public function getDockBoss()
    {
        return $this->dockBoss;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return mixed
     */
    public function getMarina()
    {
        return $this->marina;
    }



}