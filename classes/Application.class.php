<?php
/**
 * Created by PhpStorm.
 * User.class: Peter
 * Date: 18.02.2019
 * Time: 22:29
 */

class Application
{
    private $applicationID;
    private $boatID;
    private $description;
    private $status;
    private $date;
    private $dockingID;

    /**
     * Application constructor.
     * @param $applicationID
     * @param $boatID
     * @param $description
     * @param $status
     * @param $date
     */


    /**
     * @return mixed
     */
    public function getApplicationID()
    {
        return $this->applicationID;
    }

    /**
     * @return mixed
     */
    public function getBoatID()
    {
        return $this->boatID;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @return mixed
     */
    public function getDockingID()
    {
        return $this->dockingID;
    }


}