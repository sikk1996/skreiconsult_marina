<?php
/**
 * Created by PhpStorm.
 * User.class: Peter
 * Date: 18.02.2019
 * Time: 22:28
 */

class Boat
{
    private $boatID;
    private $boatName;
    private $boatRegisterNumber;
    private $manufacturer;
    private $model;
    private $length;
    private $depth;
    private $width;
    private $type;
    private $userId;

    /**
     * @return mixed
     */
    public function getBoatId()
    {
        return $this->boatID;
    }

    /**
     * @return mixed
     */
    public function getBoatName()
    {
        return $this->boatName;
    }

    /**
     * @return mixed
     */
    public function getBoatRegisterNumber()
    {
        return $this->boatRegisterNumber;
    }

    /**
     * @return mixed
     */
    public function getManufacturer()
    {
        return $this->manufacturer;
    }

    /**
     * @return mixed
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @return mixed
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * @return mixed
     */
    public function getDepth()
    {
        return $this->depth;
    }

    /**
     * @return mixed
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    public function getUserId()
    {
        return $this->userId;
    }

    public function exportToString() : string
    {

        return "Båtnavn: ".$this->boatName.", regisreringsnummer: ".$this->boatRegisterNumber.", type: ".$this->type.", produsent: ".$this->manufacturer.", model: ".$this->model.", lengde: ".$this->length."m, bredde: ".$this->width."m, dybde: ".$this->depth."m.";
    }
}