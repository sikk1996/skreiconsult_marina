<?php
/**
 * Created by PhpStorm.
 * User.class: Peter
 * Date: 18.02.2019
 * Time: 22:29
 */

class User
{

    private $firstname;
    private $lastname;
    private $age;
    private $email;
    private $confirmedMail;
    private $phone;
    private $phone2;
    private $userID;
    private $passwordHash;
    private $role;
    private $postCode;
    private $city;
    private $dateRegistered;
    private $hoursWorked;

	/**
	 * @return mixed
	 */
	public function getHoursWorked()
	{
		return $this->hoursWorked;
	}

	/**
	 * @param mixed $hoursWorked
	 */
	public function setHoursWorked($hoursWorked): void
	{
		$this->hoursWorked = $hoursWorked;
	}

    /**
     * @return mixed
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @return mixed
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @return mixed
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return mixed
     */
    public function getConfirmedMail()
    {
        return $this->confirmedMail;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @return mixed
     */
    public function getPhone2()
    {
        return $this->phone2;
    }

    /**
     * @return mixed
     */
    public function getUserID()
    {
        return $this->userID;
    }

    /**
     * @return mixed
     */
    public function getPasswordHash()
    {
        return $this->passwordHash;
    }

    /**
     * @return mixed
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @return mixed
     */
    public function getDatRegistered()
    {
        return $this->dateRegistered;
    }

    /**
     * @return mixed
     */
    public function getPostCode()
    {
        return $this->postCode;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @return mixed
     */
    public function getDateRegistered()
    {
        return $this->dateRegistered;
    }

    /**
     * @param mixed $firstname
     */
    public function setFirstname($firstname): void
    {
        $this->firstname = $firstname;
    }

    /**
     * @param mixed $lastname
     */
    public function setLastname($lastname): void
    {
        $this->lastname = $lastname;
    }

    /**
     * @param mixed $age
     */
    public function setAge($age): void
    {
        $this->age = $age;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }

    /**
     * @param mixed $confirmedMail
     */
    public function setConfirmedMail($confirmedMail): void
    {
        $this->confirmedMail = $confirmedMail;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone): void
    {
        $this->phone = $phone;
    }

    /**
     * @param mixed $phone2
     */
    public function setPhone2($phone2): void
    {
        $this->phone2 = $phone2;
    }

    /**
     * @param mixed $userID
     */
    public function setUserID($userID): void
    {
        $this->userID = $userID;
    }

    /**
     * @param mixed $passwordHash
     */
    public function setPasswordHash($passwordHash): void
    {
        $this->passwordHash = $passwordHash;
    }

    /**
     * @param mixed $role
     */
    public function setRole($role): void
    {
        $this->role = $role;
    }

    /**
     * @param mixed $postCode
     */
    public function setPostCode($postCode): void
    {
        $this->postCode = $postCode;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city): void
    {
        $this->city = $city;
    }

    /**
     * @param mixed $dateRegistered
     */
    public function setDateRegistered($dateRegistered): void
    {
        $this->dateRegistered = $dateRegistered;
    }

    public function exportToString() : string
    {
        $userID = (int)$this->userID;


        $userAddress=UserAddress_db::getUserAddressByUserId($userID);
        $postcode = "N/A";
        $city = "N/A";
        $streetAddress = "N/A";
        if ($userAddress instanceof UserAddress) {
            $postcode = $userAddress->getPostcode();
            $city = $userAddress->getCity();
            $streetAddress = $userAddress->getStreetAddress();
        }

        $response = "Registrert info: Fornavn: ".$this->firstname.", etternavn: ".$this->lastname.", epost: ".$this->email.", telefonnummer: ".$this->phone.", telefonnummer: ".$this->phone2.", postkode: ".$postcode.", gatenavn: ".$streetAddress." og by: ".$city;

        return $response;
    }
}