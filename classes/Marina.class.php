<?php
/**
 * Created by PhpStorm.
 * User.class: Peter
 * Date: 18.02.2019
 * Time: 22:29
 */

class Marina
{

    private $marinaID;
    private $marinaName;
    private $marinaBoss;

    /**
     * Marina constructor.
     * @param $marinaID
     * @param $marinaName
     * @param $marinaBoss
     */
    public function __construct($marinaID, $marinaName, $marinaBoss)
    {
        $this->marinaID = $marinaID;
        $this->marinaName = $marinaName;
        $this->marinaBoss = $marinaBoss;
    }

    /**
     * @return mixed
     */
    public function getMarinaID()
    {
        return $this->marinaID;
    }

    /**
     * @return mixed
     */
    public function getMarinaName()
    {
        return $this->marinaName;
    }

    /**
     * @return mixed
     */
    public function getMarinaBoss()
    {
        return $this->marinaBoss;
    }



}