<?php
/**
 * Created by PhpStorm.
 * User: Håkon Finstad
 * Date: 3/26/2019
 * Time: 12:51 PM
 */

require_once "helper.php";
$loginFeedback ="";

if(isset($_SESSION))
{
	$userId = $_SESSION['user_id'];
	$userRole = $_SESSION['user_role'];

	if($userRole >= 3) {
		$lastVoluntaryWork = VoluntaryWork_db::getLastTenVoluntaryWork();
		$allUsers = User_db::getAllUsers();
		$voluntaryWorkPerUser = VoluntaryWork_db::getAllVoluntaryWorkOrderedByHours();
		foreach ($allUsers as $user)
		{
			if($user instanceof User)
			{
				$hoursWorked = VoluntaryWork_db::getTotalHoursFromUser($user->getUserID());
				$user->setHoursWorked($hoursWorked);
			}
		}
		usort($allUsers, "sortUsers");

		echo $twig->render('templates/voluntaryWork.twig', array('works' => $lastVoluntaryWork, 'users' => $allUsers ,'userWork' => $voluntaryWorkPerUser));
	} else {
        header('Location: login.php?alertType="danger"&alertText="Ingen tilgang"');
    }

}

function sortUsers($a, $b)
{
	if($a instanceof User && $b instanceof User)
		return $a->getHoursWorked() - $b->getHoursWorked();
}

