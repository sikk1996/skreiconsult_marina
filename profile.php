<?php
/**
 * Created by PhpStorm.
 * User: Peter
 * Date: 18.02.2019
 * Time: 22:39
 */

require_once "helper.php";

$userId = null;
$voluntaryWorkForUser = null;
///is logged in
if (isset($_SESSION['user_role'])) {
	if ($_SESSION['user_role'] == 0) {
		echo $twig->render('templates/unverifiedUser.twig', array());
		exit();
	}

	if (isset($_GET['userId']) && $_SESSION['user_role'] >= 3) {
		$userId = $_GET['userId'];
		$user = User_db::getUserByUserId($_GET['userId']);
	} else { //Hvis ikke -> vis egen profil
		$userId = $_SESSION['user_id'];
		$user = User_db::getUserByUserId($_SESSION['user_id']);
	}

	if (isset($_POST['cancelFramleie'])) {
		$framleieApplication = Application_db::getApplicationByID($_POST['cancelFramleie']);
		$framleie = Docking_db::getFramleieByDockingID($framleieApplication->getDockingID());

		//Endre status til dockingApplication
		$boat = Boat_db::getBoatByBoatId($framleie->getBoat());
		$dockingApplication = Application_db::getApplicationByBoatID($boat->getBoatId());
		Application_db::setStatus($dockingApplication->getApplicationID(), 0);

		Application_db::deleteApplication($framleieApplication->getApplicationID());
		Docking_db::deleteFramleie($framleie->getDockingID());
	}

	if (isset($_POST['cancelFramleieAppl'])) {
		$application = Application_db::getApplicationByID($_POST['cancelFramleieAppl']);
		Application_db::deleteApplication($application->getApplicationID());
	}

	if (isset($_POST['framleie'])) {
		Application_db::newFramleieApplication($_POST['framleie']);
	}

	if (isset($_POST['deleteBoatFromDocking'])) {
		if (!$_SESSION['user_role'] > 2) {
			$tempBoat = Boat_db::getBoatByBoatId($_POST['deleteBoatFromDocking']);
			if ($tempBoat instanceof Boat) {
				if ($tempBoat->getUserId() != $_SESSION['user_id']) {
					exit();
				}
			} else {
				exit();
			}
		}
		Docking_db::deleteBoatFromDocking($_POST['deleteBoatFromDocking']);
	}

	if (isset($_POST['deleteBoat'])) {
		if (!$_SESSION['user_role'] > 2) {
			$tempBoat = Boat_db::getBoatByBoatId($_POST['deleteBoat']);
			if ($tempBoat instanceof Boat) {
				if ($tempBoat->getUserId() != $_SESSION['user_id']) {
					exit();
				}
			} else {
				exit();
			}
		}
		Boat_db::removeBoatFromUser($_POST['deleteBoat']);
	}

	if (isset($_POST['modifyBoat'])) {
		/*$_POST['modifyBoat']*/
		//TODO: Modify boat
	}

	if (isset($_POST['swapAndUserID']) && isset($_POST['dockingIDAndBoatIDAndDockID'])) {
		$array = explode("::", $_POST['dockingIDAndBoatIDAndDockID']);
		$boatID = $array[1];
		$dockingID = $array[0];

		Docking_db::deleteBoatFromDocking($boatID);
		Application_db::newApprovedDockingApplication($boatID, "Tildelt av administator");

		if (DockingAssigner::assignDockingNoApplication($boatID, $dockingID)) {
			$alertType = "alert-success";
			$alertText = "Assigned Boat to Docking";
			header('Location: profile.php?userId=' . $_POST['swapAndUserID']);
		} else {
			$alertType = "alert-danger";
			$alertText = "Assignment failed";
		}
	}

	if (isset($_POST['acceptAndUserID']) && isset($_POST['dockingIDAndBoatIDAndDockID'])) {
		$array = explode("::", $_POST['dockingIDAndBoatIDAndDockID']);
		$boatID = $array[1];
		$dockingID = $array[0];
		Application_db::newApprovedDockingApplication($boatID, "Tildelt av administator");

		if (DockingAssigner::assignDockingNoApplication($boatID, $dockingID)) {
			$alertType = "alert-success";
			$alertText = "Assigned Boat to Docking";
			header('Location: profile.php?userId=' . $_POST['acceptAndUserID']);
		} else {
			$alertType = "alert-danger";
			$alertText = "Assignment failed";
		}
	}

	if (isset($_POST['changePassword'])) {
		$oldPassword = $_POST['oldPassword'];
		$newPasswordInputOne = $_POST['newPasswordInputOne'];
		$newPasswordInputTwo = $_POST['newPasswordInputTwo'];

		if (!helperFunctions::validateString(array($oldPassword, $newPasswordInputOne, $newPasswordInputTwo))) {
			//display illegal string values
			return;
		}

		if (!helperFunctions::checkIfPasswordMatches($newPasswordInputOne, $newPasswordInputTwo)) {
			//display password does not match
			return;
		}

		if (!helperFunctions::legalPassword($newPasswordInputOne)) {
			//password is not legal
			return;
		}

		$oldPasswordHash = User_db::getPasswordHash($userId);
		helperFunctions::changePassword($oldPasswordHash, $oldPassword, $newPasswordInputOne, $userId);
	}

	$voluntaryWorkForUser = VoluntaryWork_db::getVoluntaryWorkForUser($userId);
	$applicationForUser = Application_db::getAllApplicationsFromUser($userId);
	$boats = Boat_db::getAllBoats();

	$dockings = Docking_db::getAllDockings();
	$docks = Dock_db::getAllDocks();
	$userAddress = UserAddress_db::getUserAddressByUserId($userId);
	$framleie = Docking_db::getAllFramleie();
	$users = User_db::getAllUsers();
	$allAppls = Application_db::getAllApplications();
	$unpaidInvoices = User_db::getUnpayedInvoicesFromUser($userId);
	$payedInvoices = User_db::getPayedInvoicesFromUser($userId);

	if (isset($_POST['editProfile'])) {
		fetchAndDisplayUserDataForModification($userId);
	}

	if (isset($_POST['confirm']) && $_SESSION['user_id'] == $userId && validateInputFields()) {
		modifyUserBasedOnPostReply();
		header('Location: profile.php');
	}

	if (!isset($_POST['editProfile'])) {
		echo $twig->render('templates/profile.twig', array('user' => $user, 'boats' => $boats, 'dockings' => $dockings, 'docks' => $docks, 'userAddress' => $userAddress
		, 'userWork' => $voluntaryWorkForUser, 'applications' => $applicationForUser, 'allAppls' => $allAppls, 'sessionRole' => $_SESSION['user_role']
		, 'framleieListe' => $framleie, 'users' => $users, 'unpaidInvoices' => $unpaidInvoices, 'paidInvoices' => $payedInvoices, 'activeUser'=> $_SESSION['user_id']));
	}

} else { //Hvis ikke innlogget gtfo
	header('Location: login.php?alertType="danger"&alertText="Du må logge inn for å se profil"');
}

//Henter user data å setter det inn i modify slik at man får opp eksisterende informasjon.
function fetchAndDisplayUserDataForModification($id) {
	global $twig;
	$user = User_db::getUserByUserId($id);
	$userAddress = UserAddress_db::getUserAddressByUserId($id);
	echo $twig->render('templates/adminModifyUser.twig', array('user' => $user, 'userAddress' => $userAddress));
}

//Tar Input fra skjema å setter det i database
function modifyUserBasedOnPostReply() {
	global $modifyUserFeedbackGood;
	$userid = $_POST['userIdInput'];
	$firstname = $_POST['firstNameInput'];
	$lastname = $_POST['lastNameInput'];
	$email = $_POST['emailInput'];
	$phone1 = $_POST['phoneInputOne'];
	$phone2 = $_POST['phoneInputTwo'];
	$postCode = $_POST['postcodeInput'];
	$postAddress = $_POST['postaddressInput'];
	//$city = $_POST['city'];
	if ($phone2 == "")
		$phone2 = null;
	User_db::updateUser($userid, $email, null, null, null, $firstname, $lastname, $phone1, $phone2, null, $postCode, $postAddress, null);
	$modifyUserFeedbackGood = "Bruker modifisert";
	header('Location: profile.php');
}

//Standard verifisering
function validateInputFields() {
	global $modifyUserFeedbackBad;
	if (!helperFunctions::validateEmail($_POST['emailInput'])) {
		$modifyUserFeedbackBad = "Epost er ikke gyldig";
		return false;
	}

	if (!helperFunctions::validateString(array($_POST['firstNameInput'], $_POST['lastNameInput']))) {
		$modifyUserFeedbackBad = "Fornavn eller etternavn inneholder ugyldige tegn";
		return false;
	}
	if (!helperFunctions::validateInteger($_POST['phoneInputOne']) && !helperFunctions::validatePhoneNumber($_POST['phoneInputOne'])) {
		$modifyUserFeedbackBad = "Telefonnummer skal være 8 tall";
		return false;
	}

	if (!empty($_POST['phoneInputTwo'])) {
		if (!helperFunctions::validateInteger($_POST['phoneInputTwo']) && !helperFunctions::validatePhoneNumber($_POST['phoneInputOne'])) {
			$modifyUserFeedbackBad = "Telefonnummer skal være 8 tall";
			return false;
		}
	}

	if (!helperFunctions::validateString(array($_POST['postaddressInput']))) {
		$modifyUserFeedbackBad = "Addressen inneholder ugyldige tegn";
		return false;
	}

	if (!helperFunctions::validateInteger($_POST['postcodeInput'])) {
		$modifyUserFeedbackBad = "Postkoden inneholder ugyldige tegn";
		return false;
	}
	return true;
}