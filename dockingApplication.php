<?php
$dockingApplicationFeedbackBad = "";
$dockingApplicationFeedbackGood = "";

require_once 'helper.php';

if (isset($_SESSION['user_role'])) {
    if ($_SESSION['user_role'] == 0){
        echo $twig->render('templates/unverifiedUser.twig', array());
        exit();
    }
    $userId = $_SESSION['user_id'];
    $boats = Application_db::getAllBoatsFromUserWithoutAnApplicationOrDocking($userId);

    $userRoleTemp = $_SESSION['user_role'];
} else ($userRoleTemp=0);
if($_SERVER['REQUEST_METHOD'] === 'POST' && $userRoleTemp>=1)
{

    if(isset($_POST['dockingApplicationSendInnBtn']))
    {

        $boatId = $_POST['boatID'];
        //Sjekker om det er noe søknad i databasen for den båtiden

        //Fortsett om det ikke er en søknad med båtiden

            if (count($boats) !== 0) {
                //Kontrollere om båten det søkes om er eid av den innloggede brukeren.
                try {
                    Application_db::newDockingApplication($_POST['boatID'], $_POST['descriptionInput']);
                    header('Location: profile.php');
                } catch (MeekroDBException $e) {
                    $string = "Kan ikke søke om båtplass. Båten har allerede søknadd. \\nFeilmelding: \\n" .$e->getMessage();
                    sqlErrorDisplayer($string, false);
                }


            } else {
                $dockingApplicationFeedbackBad = "Finner ingen båt med den ID-en.";
            }
    }
}
echo $twig->render('templates/dockingApplication.twig', array('dockingApplicationFeedbackBad'=>$dockingApplicationFeedbackBad, 'dockingApplicationFeedbackGood' => $dockingApplicationFeedbackGood, 'allBoatsBasedOnUser' => $boats,
));