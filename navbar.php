<nav class="navbar navbar-expand-lg navbar-dark bg-dark ">
    <a class="navbar-brand" href="index.php">Skrei Consult</a>

    <?php if(isset($_SESSION['user_name'])) : ?>
        <a class="nav navbar-text ml-auto" style="margin-right: 2em;" href="profile.php" > <?php echo ($_SESSION['user_name']) ?> </a>
    <?php endif ?>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <form method="get">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="index.php">Home </a>
                </li>
                <?php if(isset($_SESSION['user_id'])) : ?>
                    <li class="nav-item">
                        <a class="nav-link" href="profile.php">Profil</a>
                    </li>
                    <?php if(isset($_SESSION['user_role']) && $_SESSION['user_role'] >= 3) : ?>
                        <li class="nav-item">
                            <a class="nav-link" href="dockingsOverview.php">Brygger</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Båt oversikt</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="applications.php">Søknader</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="users.php">Brukere</a>
                        </li>

                    <?php endif ?>

                    <?php if(isset($_SESSION['user_role']) && $_SESSION['user_role'] == 1) :?>
                        <li class="nav-item">
                            <a class="nav-link" href="dockingApplication.php">Søk båtplass</a>
                        </li>
                    <?php endif ?>

                    <li class="nav-item">
                        <a class="nav-link" href="logout.php" name="endSession">Logg ut</a>
                    </li>
                <?php endif ?>

                <?php if(!isset($_SESSION['user_id'])) : ?>
                    <li class="nav-item">
                        <a class="nav-link" href="login.php">Logg inn</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="registerUser.php">Registrer</a>
                    </li>
                <?php endif ?>
            </ul>
        </form>

    </div>
</nav>