<?php

require_once "helper.php";

//if logged in
if (isset($_SESSION['user_role'])) {


    //if not admin
    if ($_SESSION['user_role'] < 2) {
        header('Location: index.php');


    } else { //if admin
        if ($_SESSION['user_role'] >= 3) {
            if (isset($_POST['addDocking'])) {
                $dockId = $_POST['addDocking'];
                Docking_db::addDockingAtDock($dockId);
            }
            if (isset($_POST['deleteDocking'])) {
                //Docking_db::deleteDocking($_POST['deleteDocking']);
            }
            if (isset($_POST['deleteDock'])) {
                Dock_db::deleteDock($_POST['deleteDock']);
            }
            if (isset($_POST['setAvailable'])) {
                Docking_db::setDockingAvailability($_POST['setAvailable'], 1);
            }
            if (isset($_POST['setUnavailable'])) {
                Docking_db::setDockingAvailability($_POST['setUnavailable'], 0);
            }
           if (isset($_POST['setDockAvailable'])) {
                Dock_db::setDockAvailability($_POST['setDockAvailable'], 1);
                Docking_db::setDockingAvailabilityFromDockID($_POST['setDockAvailable'], 1);
            }
            if (isset($_POST['setDockUnavailable'])) {
                Dock_db::setDockAvailability($_POST['setDockUnavailable'], 0);
                Docking_db::setDockingAvailabilityFromDockID($_POST['setDockUnavailable'], 0);
            }
            if(isset($_POST['addDock']) && isset($_POST['dockNickname']))
			{
				if(helperFunctions::validateString(array($_POST['dockNickname'])))
				{
					Docking_db::createNewDock($_POST['dockNickname']);
				}
			}

            $dockings = Docking_db::getAllDockings();
            $docks = Dock_db::getAllDocks();
            $users = User_db::getAllUsers();
            $boats = Boat_db::getAllBoats();
        } else {
            $docks = Dock_db::getAllDocksWhereDockBoss($_SESSION['user_id']);
            $dockings = array();
            foreach ($docks as $dock) {
                if ($dock instanceof Dock)
                    $dockings=array_merge(Docking_db::getAllDockingsInDock($dock->getDockID()),$dockings);
            }
            $users = User_db::getAllUsers();
            $boats = Boat_db::getAllBoats();
        }
        $framleie = Docking_db::getAllFramleie();
        echo $twig->render('templates/dockingsOverview.twig', array('dockings' => $dockings, 'docks' => $docks, 'users' => $users, 'boats' => $boats, 'framleieListe'=>$framleie));
    }
} else {
    header('Location: index.php');
}

