<?php

require_once "helper.php";

//if logged in
if (isset($_SESSION['user_role'])) {


	//if not admin
	if ($_SESSION['user_role'] < 2) {
		header('Location: index.php');
	}
	$boatsWithOwner = Boat_db::getAllBoatsWithOwner();

	echo $twig->render('templates/BoatsOverview.twig', array('boatsWithOwner' => $boatsWithOwner));
}