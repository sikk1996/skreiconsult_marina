<?php
/**
 * Created by PhpStorm.
 * User: Håkon Finstad
 * Date: 2/27/2019
 * Time: 5:34 PM
 */

class helperFunctions
{
	public static function checkIfPasswordMatches($passwordInputOne, $passwordInputTwo)
	{

		return ($passwordInputOne == $passwordInputTwo);
	}

	public static function validateEmail($email)
	{
		if(self::isInputEmpty($email))
        {

        }
		return filter_var($email, FILTER_VALIDATE_EMAIL);
	}
	public static function validateString($strings)
	{

		$regex = '/^[A-Za-z0-9"\'.,!?æøåÆØÅ_ -]+$/';
		foreach ($strings as $string)
		{
			if(!self::isInputEmpty($string))
			{
				return false;
			}
			if(!preg_match($regex, $string))
            {
                return false;
            }
		}
		return true;
	}

	public static function legalPassword($pwd)
	{
		return strlen($pwd) >= 6;
	}
	public static function validateInteger($integer)
	{
	    $result = (filter_var($integer, FILTER_VALIDATE_INT));
        if ($result == $integer) {
            return true;
        } else {
            return false;
        }

	}
	public static function validateFloat($float)
    {
        return (filter_var($float, FILTER_VALIDATE_FLOAT));
    }

	public static function isInputEmpty($input)
	{
        if(!empty($input)) {
            return true;
        }else{
            return false;
        }
    }

    public static function checkLengthOfInput_Strings($input)
    {
        if(strlen($input) < 25) {
            return true;
        } else{
            return false;
        }
    }

	public static function validatePhoneNumber($input)
	{
        $number = strlen($input);
        if ($number==8) {
            return true;
        }
		return false;
	}

	public static function encryptPassword($input)
    {
        return password_hash($input, PASSWORD_DEFAULT);
        //return crypt($input, '$2a$09$sykestehashennoensinneskjera$');
        /*$saltValue = uniqid(mt_rand(), false);
        $salt = '$2y$09$' . $saltValue.'$';
        return crypt($input, $salt);*/
    }
    public static function comparePasswordHash($OldPasswordHash, $oldPassword)
    {

        if (password_verify($oldPassword,$OldPasswordHash))
        {
            return true;
        }
        return false;
    }

    public static function compareUserRights($rightRequirement, $userRight)
	{
		if($userRight >= $rightRequirement)
			return true;
		return false;
	}
	public static function changePassword($oldPasswordHashed, $oldpassword, $newPassword, $userId)
	{
		if(!self::comparePasswordHash($oldPasswordHashed, $oldpassword))
		{
		    die("password does not match");
			return 0;
		}

			//change password
			$newPasswordHash = self::encryptPassword($newPassword);
			User_db::setUserPassword($userId, $newPasswordHash);
			return 1;
	}
    public static function randomPassword($length) {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < $length; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }


}