<?php

/**
 * Created by PhpStorm.
 * User: berge
 * Date: 20.03.2019
 * Time: 00.34
 */
class UserAddress_db
{

    public static function getUserAddressByUserId($UserId): UserAddress
    {
        try {
            $mysqli_result = DB::queryRaw("select streetAddress, city, tbl_postalAddress.postcode from tbl_user left join tbl_userAddress on tbl_user.userID = tbl_userAddress.userID left join tbl_postalAddress on tbl_userAddress.postcode = tbl_postalAddress.postcode WHERE tbl_user.userID=%i;", $UserId);
            $result = mysqli_fetch_object($mysqli_result, UserAddress::class);
            if ($result instanceof UserAddress) {
                return $result;
            }

        } catch (InvalidArgumentException $exception) {
            print ($exception->getMessage() . PHP_EOL);
        } catch (MeekroDBException $exception1) {
            sqlErrorDisplayer($exception1, true);
        }
        return null;
    }
}