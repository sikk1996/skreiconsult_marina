<?php
/**
 * Created by PhpStorm.
 * User: Emma
 * Date: 2019-03-06
 * Time: 11:32
 */

class DockingAssigner
{

    public static function assignDocking($applicationID, $dockingID)
    {
        try {
            self::manageDatabase($applicationID, $dockingID);
            return true;
        } catch (MeekroDBException $exception) {
            $string = "Klarte ikke tildele båtplass. Vennligst kontakt systemadministrator hvis problemet vedvarer.\\nFeilmelding: " . $exception->getMessage();
            sqlErrorDisplayer($string, false);
        }
        return false;
    }

    public static function assignDockingNoApplication($boatID, $dockingID)
    {
        try {
            self::updateDatabase($boatID, $dockingID);
            return true;
        } catch (MeekroDBException $exception) {
            sqlErrorDisplayer($exception, true);
        }
        return false;
    }

    private static function manageDatabase($applicationID, $dockingID)
    {
        try {
            self::updateDatabase(self::getBoatID($applicationID), $dockingID);
        } catch (MeekroDBException $exception) {
            sqlErrorDisplayer($exception, true);
        }
        //DB::delete('tbl_dockingApplication',"applicationID=%i",$applicationID);
    }

    private static function getBoatID($applicationID)
    {
        try {
            return DB::queryFirstField("SELECT boatID FROM tbl_dockingApplication WHERE applicationID=%i", $applicationID);
        } catch (MeekroDBException $exception) {
            sqlErrorDisplayer($exception, true);
        }
        return null;
    }

    private static function updateDatabase($boatID, $dockingID)
    {
        try {
            DB::queryRaw("UPDATE `tbl_dockings` SET `boat` = %i WHERE `tbl_dockings`.`dockingID` = %i;", $boatID, $dockingID);
        } catch (MeekroDBException $exception) {
            sqlErrorDisplayer($exception, true);
        }
    }

    private static function checkDockingIsFree($dockingId)
    {
        try {
            return (DB::queryFirstField("SELECT boat FROM tbl_dockings WHERE dockingID=%i", $dockingId) == null);
        } catch (MeekroDBException $exception) {
            sqlErrorDisplayer($exception, true);
        }
        return null;
    }
}