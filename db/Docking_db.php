<?php
/**
 * Created by PhpStorm.
 * User.class: Peter
 * Date: 18.02.2019
 * Time: 22:30
 */

class Docking_db
{
    public static function createNewDocking($dockID, $boatID, $subleaseToUser, $subleaseBoat)
    {
        DB::insert('tbl_dockings', array(
            'dock' => $dockID,
            'boat' => $boatID,
            'subleaseToUser' => $subleaseToUser,
            'subleaseBoat' => $subleaseBoat
        ));
    }

    public static function getAvailableAndAccessibleDockings()
    {
        //SELECT * FROM `tbl_dockings` where boat is null and status = 1 ORDER BY `tbl_dockings`.`dock` ASC, `tbl_dockings`.`dockingID` ASC

        $dockings[] = [];
        try{
            $mysqli_result = DB::queryRaw("SELECT * FROM `tbl_dockings` where boat is null and `tbl_dockings`.`status` = 1 ORDER BY `tbl_dockings`.`dock` ASC, `tbl_dockings`.`dockingID` ASC");
            while ($obj = mysqli_fetch_object($mysqli_result, Docking::class))
            {
                $dockings[] = $obj;
            }
            return $dockings;
        } catch (InvalidArgumentException $exception) {
            print ($exception->getMessage() . PHP_EOL);
        } catch (MeekroDBException $exception) {
            sqlErrorDisplayer($exception, true);
        }
        return null;
    }

    public static function getAvailableDockings()
    {
        //SELECT * FROM `tbl_dockings` where boat is null ORDER BY `tbl_dockings`.`dock` ASC, `tbl_dockings`.`dockingID` ASC

        $dockings[] = [];
        try {
            $mysqli_result = DB::queryRaw("SELECT * FROM `tbl_dockings` where boat is null ORDER BY `tbl_dockings`.`dock` ASC, `tbl_dockings`.`dockingID` ASC");
            while ($obj = mysqli_fetch_object($mysqli_result, Docking::class)) {
                $dockings[] = $obj;
            }
            return $dockings;
        } catch (InvalidArgumentException $exception) {
            print ($exception->getMessage() . PHP_EOL);
        } catch (MeekroDBException $exception) {
            sqlErrorDisplayer($exception, true);
        }
        return null;
    }

    public static function getDockingByID($dockingID): Docking
    {
        try {
            $mysqli_result = DB::queryRaw("SELECT * FROM `tbl_dockings` where dockingID=%i", $dockingID);
            if ($mysqli_result->num_rows == 0) {
                return new Docking();
            }
            $docking = mysqli_fetch_object($mysqli_result, Docking::class);
            if ($docking instanceof Docking) {
                return $docking;
            }

        } catch (InvalidArgumentException $exception) {
            print ($exception->getMessage() . PHP_EOL);
        } catch (MeekroDBException $exception1) {
            sqlErrorDisplayer($exception1, true);
        }
        return null;
    }

    public static function updateDocking($dockID, $boatID, $subleaseToUser, $subleaseBoat)
    {
        try {
            DB::update('tbl_dockings', array(
                'dock' => $dockID,
                'boat' => $boatID,
                'subleaseToUser' => $subleaseToUser,
                'subleaseBoat' => $subleaseBoat
            ));
        } catch (MeekroDBException $exception) {
            sqlErrorDisplayer($exception, true);
        }

    }

    public static function removeDockingFromDock($dockID)
    {
        try {
            DB::delete('tbl_dockings', "dock=%i", $dockID);
        } catch (MeekroDBException $exception) {
            sqlErrorDisplayer($exception, true);
        };
    }

    public static function getAllDockings()
    {
        $dockings[] = [];
        try{
            $mysqli_result = DB::queryRaw("SELECT * FROM tbl_dockings");
            while ($obj = mysqli_fetch_object($mysqli_result, Docking::class)) {
                $dockings[] = $obj;
            }
            return $dockings;
        } catch (InvalidArgumentException $exception) {
            print ($exception->getMessage() . PHP_EOL);
        } catch (MeekroDBException $exception1) {
            sqlErrorDisplayer($exception1, true);
        }
        return null;
    }

    public static function deleteFramleie($dockingID)
    {
        DB::delete('tbl_framleie', "dockingID=%i", $dockingID);
    }

    public static function createNewFramleie($dockingApplID,$framleieApplID)
    {
        try {
            $dockingAppl = Application_db::getApplicationByID($dockingApplID);
            $framleieAppl = Application_db::getApplicationByID($framleieApplID);
            $boat = Boat_db::getBoatByBoatId($dockingAppl->getBoatID());
            DB::insert("tbl_framleie", array(
                'dockingID' => $framleieAppl->getDockingID(),
                'boat' => $dockingAppl->getBoatID(),
                'framleieUserID' => $boat->getUserId()
            ));
        } catch (InvalidArgumentException $exception) {
            print ($exception->getMessage() . PHP_EOL);
        } catch (MeekroDBException $exception) {
            sqlErrorDisplayer($exception, true);
        }
    }

    public static function getAllFramleie()
    {
        $dockings[] = [];
        try {
            $mysqli_result = DB::queryRaw("SELECT * FROM tbl_framleie");
            while ($obj = mysqli_fetch_object($mysqli_result, Framleie::class)) {
                $dockings[] = $obj;
            }
            return $dockings;
        } catch (InvalidArgumentException $exception) {
            print ($exception->getMessage() . PHP_EOL);
        } catch (MeekroDBException $exception1) {
            sqlErrorDisplayer($exception1, true);
        }
        return null;
    }
    public static function getFramleieByDockingID($dockingID) :Framleie
    {
        try {
            $mysqli_result = DB::queryRaw("SELECT * FROM `tbl_framleie` where dockingID=%i", $dockingID);
            if ($mysqli_result->num_rows == 0) {
                return new Framleie();
            }
            $framleie = mysqli_fetch_object($mysqli_result, Framleie::class);
            if ($framleie instanceof Framleie) {
                return $framleie;
            }

        } catch (InvalidArgumentException $exception) {
            print ($exception->getMessage() . PHP_EOL);
        } catch (MeekroDBException $exception1) {
            sqlErrorDisplayer($exception1, true);
        }
        return null;
    }
	public static function getFramleieByBoatID($boatID) :Framleie
	{
		try {
			$mysqli_result = DB::queryRaw("SELECT * FROM `tbl_framleie` where boat=%i", $boatID);
			if ($mysqli_result->num_rows == 0) {
				return new Framleie();
			}
			$framleie = mysqli_fetch_object($mysqli_result, Framleie::class);
			if ($framleie instanceof Framleie) {
				return $framleie;
			}

		} catch (InvalidArgumentException $exception) {
			print ($exception->getMessage() . PHP_EOL);
		} catch (MeekroDBException $exception1) {
			sqlErrorDisplayer($exception1, true);
		}
		return null;
	}

    public static function getAllDockingsInDock($dockID)
    {
        try {
            $dockingArray[] = [];
            $results = DB::queryRaw("SELECT * FROM tbl_dockings WHERE dock=%i", $dockID);
            while ($obj = $results->fetch_object(Docking::class)) {
                $dockingArray[]=$obj;
            }
            return $dockingArray;
        } catch (MeekroDBException $exception) {
            sqlErrorDisplayer($exception, true);
        }
        return null;
    }

    public static function getAllDockingsInMarina($marinaID)
    {
        try {
            $dockingArray[] = array();
            $results = DB::query("SELECT dockingID, dock, boat, subleaseToUser, subleaseBoat, dockBoss FROM tbl_dockings INNER JOIN tbl_docks WHERE tbl_docks.dockID = tbl_dockings.dock AND tbl_docks.marina=%i", $marinaID);
            while ($obj = $results->fetch_object('Docking')) {
                array_push($dockingArray, $obj);
            }
            return $dockingArray;
        } catch (MeekroDBException $exception) {
            sqlErrorDisplayer($exception, true);
        }
        return null;
    }

    public static function addDockingAtDock($dockID)
    {
        try {
            DB::insert("tbl_dockings", array(
                'dock' => $dockID
            ));
        } catch (InvalidArgumentException $exception) {
            print ($exception->getMessage() . PHP_EOL);
        } catch (MeekroDBException $exception) {
            sqlErrorDisplayer($exception, true);
        }
    }

    public static function setDockingAvailability($dockingID, $available)
    {
        try {
            DB::update('tbl_dockings', array(
                'status' => $available
            ), 'dockingID=%i', $dockingID);
        } catch (InvalidArgumentException $exception) {
            print ($exception->getMessage() . PHP_EOL);
        } catch (MeekroDBException $exception) {
            sqlErrorDisplayer($exception, true);
        }
    }

    public static function setDockingAvailabilityFromDockID($dockID, $available)
    {
        try {
            DB::update('tbl_dockings', array(
                'status' => $available
            ), 'dock=%i', $dockID);
        } catch (MeekroDBException $exception) {
            sqlErrorDisplayer($exception, true);
        }
        return null;
    }

    public static function deleteBoatFromDocking($boatID)
    {
        $dockingID = null;
        try {
            DB::update('tbl_dockings', array(
                'boat' => null
            ), 'boat=%i', $boatID);
            try {
                Application_db::deleteApplicationWithBoat($boatID);
            } catch (MeekroDBException $exception) {
                $string = "Klarte ikke å slette søknadden. Venligst kontakt systemadminisrator\\nå oppgi følgende feilkode hvis prolemet vedvarer: \\n" . $exception->getMessage();
                sqlErrorDisplayer($string, false);

            }
        } catch (InvalidArgumentException $exception) {
            print ($exception->getMessage() . PHP_EOL);
        } catch (MeekroDBException $exception) {
            $stringtwo = "Båten bli ikke fjernet fra båtplassen. Venligst kontakt systemadminisrator\\nå oppgi følgende feilkode hvis prolemet vedvarer: \\n" . $exception->getMessage();
            sqlErrorDisplayer($stringtwo, false);
        }
    }

    public static function createNewDock($dockName)
    {
        try {
            DB::insert("tbl_docks", array(
                'marina' => 1,
                'nickname' => $dockName
            ));
        } catch (MeekroDBException $exception) {
            $string = "Kan ikke lagre brygge. Prøv et annet navn på bryggen.\\nVenligst kontakt systemadminisrator\\nå oppgi følgende feilkode hvis prolemet vedvarer: \\n" . $exception->getMessage();
            sqlErrorDisplayer($string, false);
        }
    }
}