<?php
/**
 * Created by PhpStorm.
 * User.class: Peter
 * Date: 18.02.2019
 * Time: 22:30
 */

class Dock_db
{

    public static function createNewDock($marinaID, $nickname, $dockBoss)
    {
        try {
            DB::insert('tbl_docks', array(
                'nickname' => $nickname,
                'marina' => $marinaID,
                'dockBoss' => $dockBoss,

            ));
        } catch (MeekroDBException $exception) {
            sqlErrorDisplayer($exception, true);
        }
    }

    public static function getDocksWithAvailableAndAccessibleDockings()
    {
        //SELECT DISTINCT tbl_docks.* FROM `tbl_dockings` left join tbl_docks on dock=tbl_docks.dockID where boat is null and status = 1 ORDER BY `dockID` ASC

        $docks[] = [];
        try{
            $mysqli_result = DB::queryRaw("SELECT DISTINCT tbl_docks.* FROM `tbl_dockings` left join tbl_docks on dock=tbl_docks.dockID where boat is null and tbl_dockings.status = 1 ORDER BY `dockID` ASC");
            while ($obj = mysqli_fetch_object($mysqli_result, Dock::class))
            {
                $docks[] = $obj;
            }
        } catch (InvalidArgumentException $exception) {
            print ($exception->getMessage() . PHP_EOL);
        } catch (MeekroDBException $e) {
            sqlErrorDisplayer($e, true);
        }

        return $docks;
    }

    public static function getDocksWithAvailableDockings()
    {
        //SELECT DISTINCT tbl_docks.* FROM `tbl_dockings` left join tbl_docks on dock=tbl_docks.dockID where boat is null and status = 1 ORDER BY `dockID` ASC

        $docks[] = [];
        try {
            $mysqli_result = DB::queryRaw("SELECT DISTINCT tbl_docks.* FROM `tbl_dockings` left join tbl_docks on dock=tbl_docks.dockID where boat is null ORDER BY `dockID` ASC");
            while ($obj = mysqli_fetch_object($mysqli_result, Dock::class)) {
                $docks[] = $obj;
            }
            return $docks;
        } catch (InvalidArgumentException $exception) {
            print ($exception->getMessage() . PHP_EOL);
        } catch (MeekroDBException $exception1) {
            sqlErrorDisplayer($exception1, true);
        }
    }


    public static function updateDock($marinaID, $nickname, $dockBoss, $status, $marina)
    {
        try {
            DB::update('tbl_docks', array(
                'nickname' => $nickname,
                'dockBoss' => $dockBoss,
                'status' => $status,
                'marina' => $marina,

            ), "marina=%i", $marinaID);
        } catch (MeekroDBException $exception) {
            $string = "Kunne ikke oppdatere ";
            sqlErrorDisplayer($exception, true);
        }
    }

    public static function updateDockBoss($dockID, $dockBoss)
    {
        try {
            DB::update('tbl_docks', array(
                'dockBoss' => $dockBoss,

            ), "dockID=%i", $dockID);
        } catch (MeekroDBException $exception) {
            sqlErrorDisplayer($exception, true);
        }
    }

    public static function removeDockFromMarina($marinaID, $dockID)
    {
        try {
            $where = new WhereClause('and');
            $where->add("marina=%i", $marinaID);
            $where->add("dockID=%i", $dockID);
            DB::delete('tbl_boat', $where);
        } catch (MeekroDBException $exception) {
            sqlErrorDisplayer($exception, true);
        }
    }

    public static function getAllDocks()
    {
        $docks[] = [];
        try {
            $mysqli_result = DB::queryRaw("SELECT * FROM tbl_docks ORDER BY nickname ASC");
            while ($obj = mysqli_fetch_object($mysqli_result, Dock::class)) {
                $docks[] = $obj;
            }
            return $docks;
        } catch (MeekroDBException $exception) {
            sqlErrorDisplayer($exception, true);
        }
    }

    public static function getAllDocksInMarina($marinaID)
    {
        try {
            $dockArray[] = array();
            $results = DB::query("SELECT * FROM tbl_docks WHERE marina=%i", $marinaID);
            while ($obj = $results->fetch_object('Dock')) {
                array_push($dockArray, $obj);
            }
            return $dockArray;
        } catch (MeekroDBException $exception) {
            sqlErrorDisplayer($exception, true);
        }
    }

    public static function getAllDocksWhereDockBoss($boss)
    {
        try {
            $dockArray[] = [];
            $results = DB::queryRaw("SELECT * FROM tbl_docks WHERE dockBoss=%i", $boss);
            while ($obj = mysqli_fetch_object($results, Dock::class)) {
                $dockArray[] = $obj;
            }
            return $dockArray;
        } catch (MeekroDBException $exception) {
            sqlErrorDisplayer($exception, true);
        }
    }

    public static function getAllDocksWithoutDockBoss()
    {
        try {
            $dockArray[] = [];
            $results = DB::queryRaw("SELECT * FROM tbl_docks where dockBoss is null");
            while ($obj = mysqli_fetch_object($results, Dock::class)) {
                $dockArray[] = $obj;
            }
            return $dockArray;
        } catch (MeekroDBException $exception) {
            sqlErrorDisplayer($exception, true);
        }
    }

    public static function setDockAvailability($dockID, $available)
    {
        try {
            DB::update('tbl_docks', array(
                'status' => $available
            ), 'dockID=%i', $dockID);
        } catch (InvalidArgumentException $exception) {
            print ($exception->getMessage() . PHP_EOL);
        }
    }

    public static function deleteDock($dockID)
    {
        try {
            DB::delete('tbl_docks', "dockID=%i", $dockID);
        } catch (MeekroDBException $exception) {
            sqlErrorDisplayer($exception, true);
        };
    }

    public static function getAllDockBosses()
    {
        try {
            $marinaBossArray[] = [];
            $results = DB::queryRaw("SELECT * FROM tbl_user where role = 2");
            while ($obj = mysqli_fetch_object($results, User::class)) {
                $marinaBossArray[] = $obj;
            }
            return $marinaBossArray;
        } catch (MeekroDBException $exception) {
            sqlErrorDisplayer($exception, true);
        }
        return null;
    }
}