<?php
/**
 * Created by PhpStorm.
 * User: Håkon Finstad
 * Date: 4/9/2019
 * Time: 12:56 PM
 */

class Settings_db
{

	public static function getSettings()
	{
		return DB::queryFirstRow("SELECT * FROM tbl_globalSettings");
	}
	public static function setMarinaName($marinaName)
	{
		DB::update("tbl_globalSettings", array(
			'marinaName' => $marinaName
		), 'ID=%i', 1);
	}
	public static function setMemberAmount($newAmount)
    {
        DB::update("tbl_globalSettings", array(
            'memberPayment' => $newAmount
        ), 'ID=%i', 1);
    }
    public static function setDockingPaymentAmount($newAmount)
    {
        DB::update("tbl_globalSettings", array(
            'dockingPayment' => $newAmount
        ), 'ID=%i', 1);
    }
}