<?php
/**
 * Created by PhpStorm.
 * User.class: Peter
 * Date: 18.02.2019
 * Time: 22:30
 */

class User_db {
	public static function newUser($email, $password, $firstname, $lastname
		, $phone, $phone2, $address, $postcode) {
		try {
			DB::insert("tbl_user", array(
				'email' => $email,
				'passwordHash' => $password,
				'role' => 0,
				'comfirmedMail' => 0,
				'firstname' => $firstname,
				'lastname' => $lastname,
				'phone' => $phone,
				'phone2' => $phone2
			));
		} catch (MeekroDBException $exception) {
			$string = "Klarte ikke å legge inn bruker. Vennligst prøv igjen senere. \\nVenligst kontakt systemadminisrator\\nå oppgi følgende feilkode hvis prolemet vedvarer:\\n" . $exception->getMessage();
			sqlErrorDisplayer($string, false);
		}
		try {
			$id = DB::insertId();
			DB::insert("tbl_userAddress", array(
				'userID' => $id,
				'streetAddress' => $address,
				'postcode' => $postcode

			));
			$userID = self::getUserByEmail($email)->getUserID();
			DB::insert("tbl_verificationKey", array('userId' => $userID, 'keyValue' => helperFunctions::randomPassword(25)));
		} catch (MeekroDBException $exception) {
			sqlErrorDisplayer($exception, true);
		}
	}


	public static function getVerificationKeyByUserId($userId) {
		try {
			$result = DB::queryRaw("SELECT keyValue FROM tbl_verificationKey WHERE userId=%i", $userId);
			$row = $result->fetch_assoc();
			$verificationKey = $row['keyValue'];
			return $verificationKey;
		} catch (InvalidArgumentException $exception) {
			print ($exception->getMessage() . PHP_EOL);
		} catch (MeekroDBException $exception1) {
			sqlErrorDisplayer($exception1, true);
		}

		return null;
	}

	public static function removeVerificationKeyByUserId($userId){
        try {
            DB::query("DELETE FROM `tbl_verificationKey` WHERE userId=%i", $userId);
        } catch (MeekroDBException $exception) {
            self::setDatePaid($exception, true);
        }
    }

	public static function getUserByEmail($email): User {

		try {
			$mysqli_result = DB::queryRaw("SELECT * FROM tbl_user WHERE email=%s", $email);
			if ($mysqli_result->num_rows == 0) {
				return new User();
			}
			$user = mysqli_fetch_object($mysqli_result, User::class);
			if ($user instanceof User) {
				return $user;
			}

		} catch (InvalidArgumentException $exception) {
			print ($exception->getMessage() . PHP_EOL);
		} catch (MeekroDBException $exception1) {
			sqlErrorDisplayer($exception1, true);
		}
		return null;
	}

	public static function setUserPassword($userId, $passwordHash) {
		try {
			DB::queryRaw("UPDATE tbl_user SET passwordHash=%s WHERE userID=%i", $passwordHash, $userId);
		} catch (InvalidArgumentException $exception) {
			die ($exception->getMessage());
		} catch (MeekroDBException $exception1) {
			sqlErrorDisplayer($exception1, true);
		}
	}

	public static function getPasswordHash($userid) {
		try {
			$result = DB::queryRaw('SELECT passwordHash FROM tbl_user WHERE userID=%s', $userid);
			$row = $result->fetch_assoc();
			$passwordHash = $row['passwordHash'];
			return $passwordHash;
		} catch (MeekroDBException $exception) {
			$string = "Klarte ikke hente passord fra systemt.";
			sqlErrorDisplayer($string, true);
		}
		return null;
	}

	public static function setUserRights($userId, $role) {
		try {
			DB::queryRaw("UPDATE tbl_user SET role=%i WHERE userID=%i", $role, $userId);
		} catch (InvalidArgumentException $exception) {
			die ($exception->getMessage());
		} catch (MeekroDBException $exception) {
			sqlErrorDisplayer($exception, true);
		}
	}

	public static function getAllUsersByDock($dockId) {
		$users[] = [];
		try {
			$mysqli_result = DB::queryRaw("SELECT DISTINCT tbl_user.* FROM `tbl_dockings` right join tbl_boat on tbl_dockings.boat=tbl_boat.boatID left join tbl_user on tbl_boat.userId = tbl_user.userID where dock=%i", $dockId);
			while ($obj = mysqli_fetch_object($mysqli_result, User::class)) {
				$users[] = $obj;
			}
			return $users;
		} catch (InvalidArgumentException $exception) {
			print ($exception->getMessage() . PHP_EOL);
		} catch (MeekroDBException $exception) {

			sqlErrorDisplayer($exception, true);
		}
		return null;
	}

	public static function createInvoice($id, $amount) {
		try {
			DB::insert('tbl_invoice', array(
				'userID' => $id,
				'outstanding' => $amount,
				'date' => date("Y/m/d")
			));
		} catch (InvalidArgumentException $exception) {
			print($exception->getMessage() . PHP_EOL);
		}

	}

	public static function getAllUsers() {
		$users[] = [];
		try {
			$mysqli_result = DB::queryRaw("SELECT * FROM tbl_user");
			while ($obj = mysqli_fetch_object($mysqli_result, User::class)) {
				$users[] = $obj;
			}
		} catch (InvalidArgumentException $exception) {
			print ($exception->getMessage() . PHP_EOL);
		} catch (MeekroDBException $exception) {
			sqlErrorDisplayer($exception, true);
		}
		return $users;
	}

	public static function getAllVerifiedUsers() {
		try {
			$mysqli_result = DB::query("SELECT * FROM tbl_user WHERE role >= 1");
		} catch (InvalidArgumentException $exception) {
			print ($exception->getMessage() . PHP_EOL);
		} catch (MeekroDBException $exception) {
			sqlErrorDisplayer($exception, true);
		}
		return $mysqli_result;
	}

	public static function getAllUsersJSON() {
		$users[] = array();
		try {
			$sth = DB::queryRaw("SELECT * FROM `view_UsersNoPasswordHashWithWork`");
			$users = mysqli_fetch_all($sth, MYSQLI_ASSOC);
		} catch (InvalidArgumentException $exception) {
			print ($exception->getMessage() . PHP_EOL);
		} catch (MeekroDBException $exception) {
			sqlErrorDisplayer($exception, true);
		}
		return json_encode($users);
	}

	public static function getAllApplicants() {
		try {

			//Insert in Try block

		} catch (MeekroDBException $exception) {
			sqlErrorDisplayer($exception, true);
		}
	}

	public static function getUsersWithDocking() {
		try {

			//Insert in Try block

		} catch (MeekroDBException $exception) {
			sqlErrorDisplayer($exception, true);
		}
	}

	public static function createNewInvoiceForUser($userId, $amount) {
		try {
			DB::insert('tbl_invoice', array(
				'userID' => $userId,
				'statusPaied' => 0,
				'outstanding' => $amount
			));
		} catch (MeekroDBException $exception) {
			sqlErrorDisplayer($exception, true);
		}
	}

	public static function getUnpayedInvoicesFromUser($userId) {
		try {
			$result = DB::query("SELECT * FROM tbl_invoice WHERE userID=%i AND datePaid IS NULL ORDER BY date", $userId);
			return $result;
		} catch (MeekroDBException $exception) {
			sqlErrorDisplayer($exception, true);
		}
	}

	public static function getAllInvoices() {
		try {
			$result = DB::query("SELECT * FROM invoiceWithUser ORDER BY invoiceID DESC");
			return $result;
		} catch (MeekroDBException $exception) {
			sqlErrorDisplayer($exception, true);
		}
	}

	public static function getPayedInvoicesFromUser($userId) {
		try {
			$result = DB::query("SELECT * FROM tbl_invoice WHERE userID=%i AND datePaid IS NOT NULL ORDER BY date", $userId);
			return $result;
		} catch (MeekroDBException $exception) {
			sqlErrorDisplayer($exception, true);
		}
	}

	public static function setDatePaid($invoiceID, $date) {
		DB::query("UPDATE tbl_invoice SET datePaid =%s WHERE invoiceID =%i", $date, $invoiceID);

	}

	public static function getUsersWithoutDocking() {
		try {

			//Insert in Try block

		} catch (MeekroDBException $exception) {
			sqlErrorDisplayer($exception, true);
		}
	}

	//AJAX FUNCTION
	public static function getPostalCity($postalCode) {
		try {
			$city = "";
			$result = DB::query("SELECT city FROM tbl_postalAddress WHERE postcode=%i", $postalCode);
			foreach ($result as $row) {
				$city = $row['city'];
			}
			echo $city;
		} catch (MeekroDBException $exception) {
			sqlErrorDisplayer($exception, true);
		}
	}

	public static function getUserByUserId($UserId): User {
		try {
			$mysqli_result = DB::queryRaw("SELECT * FROM tbl_user WHERE userID=%i", $UserId);
			$result = mysqli_fetch_object($mysqli_result, User::class);
			if ($result instanceof User) {
				return $result;
			}
		} catch (InvalidArgumentException $exception) {
			print ($exception->getMessage() . PHP_EOL);
		} catch (MeekroDBException $exception) {
			sqlErrorDisplayer($exception, true);
		}
		return null;
	}

	public static function deleteUser(int $userID) {
		try {
			//DB::delete('tbl_user', 'userID=%i', $userID);
			DB::query("DELETE FROM tbl_user WHERE userID = %i", $userID);
		} catch (MeekroDBException $exception) {
			sqlErrorDisplayer($exception, true);
		}
	}

	public function addUser($userID, $email, $passwordHash, $role, $confirmedMail, $firstname, $lastname, $phone1, $phone2, $dateRegistered) {
		try {
			$user = User($userID, $email, $passwordHash, $role, $confirmedMail, $firstname, $lastname, $phone1, $phone2, $dateRegistered);

			DB::insert('tbl_user', array(
				'userID' => $user->getUserID(),
				'email' => $user->getEmail(),
				'passwordHash' => $user->getPasswordHash(),
				'role' => $user->getRole(),
				'confirmedEmail' => $user->getConfirmedMail(),
				'firstname' => $user->getFirstname(),
				'lastname' => $user->getLastname(),
				'phone' => $user->getPhone(),
				'phone2' => $user->getPhone2(),
				'dateRegistered' => $user->getAge()
			));
			return $user;
		} catch (MeekroDBException $exception) {
			sqlErrorDisplayer($exception, true);
		}
		return null;
	}

	public static function updateUser($userID, $email, $passwordHash, $role, $confirmedMail, $firstname, $lastname, $phone, $phone2, $dateRegistered, $postCode, $address, $city) {
		try {
			if ($userID == null) {
				return;
			}
			if ($email == null) {
				$result = DB::query("SELECT email FROM tbl_user WHERE userID=%i", $userID);
				$email = $result[0]['email'];
			}
			if ($passwordHash == null) {
				$result = DB::query("SELECT passwordHash FROM tbl_user WHERE userID=%i", $userID);
				$passwordHash = $result[0]['passwordHash'];
			}
			if ($role == null) {
				$result = DB::query("SELECT role FROM tbl_user WHERE userID=%i", $userID);
				$role = $result[0]['role'];
			}
			if ($confirmedMail == null) {
				$result = DB::query("SELECT comfirmedMail FROM tbl_user WHERE userID=%i", $userID);
				$confirmedMail = $result[0]['comfirmedMail'];
			}
			if ($firstname == null) {
				$result = DB::query("SELECT firstname FROM tbl_user WHERE userID=%i", $userID);
				$firstname = $result[0]['firstname'];
			}
			if ($lastname == null) {
				$result = DB::query("SELECT lastname FROM tbl_user WHERE userID=%i", $userID);
				$lastname = $result[0]['firstname'];
			}
			if ($phone == null) {
				$result = DB::query("SELECT phone FROM tbl_user WHERE userID=%i", $userID);
				$phone = $result[0]['phone'];
			}
			if ($phone2 == null) {
				$result = DB::query("SELECT phone2 FROM tbl_user WHERE userID=%i", $userID);
				$phone = $result[0]['phone2'];
			}
			if ($dateRegistered == null) {
				$result = DB::query("SELECT dateRegistered FROM tbl_user WHERE userID=%i", $userID);
				$dateRegistered = $result[0]['dateRegistered'];
			}
			if ($postCode == null) {
				$result = DB::query("SELECT postcode FROM tbl_userAddress WHERE userID=%i", $userID);
				$postCode = $result[0]['postcode'];
			}
			if ($address == null) {
				$result = DB::query("SELECT streetAddress FROM tbl_userAddress WHERE userID=%i", $userID);
				$address = $result[0]['streetAddress'];
			}
			if ($city == null) {
				$result = DB::query("SELECT city FROM tbl_postalAddress WHERE postcode=%i", $postCode);
				$city = $result[0]['city'];
			}
			DB::update('tbl_user', array(
				'userID' => $userID,
				'email' => $email,
				'passwordHash' => $passwordHash,
				'role' => $role,
				'comfirmedMail' => $confirmedMail,
				'firstname' => $firstname,
				'lastname' => $lastname,
				'phone' => $phone,
				'phone2' => $phone2,
				'dateRegistered' => $dateRegistered,

			), 'userID=%i', $userID);
			DB::update('tbl_userAddress', array(
				'postcode' => $postCode,
				'streetAddress' => $address
			), 'userID=%i', $userID);
		} catch (MeekroDBException $exception) {
			sqlErrorDisplayer($exception, true);
		}

	}
}