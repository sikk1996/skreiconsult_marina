<?php
/**
 * Created by PhpStorm.
 * User.class: Peter
 * Date: 18.02.2019
 * Time: 22:30
 */

class Marina_db
{

    public static function createNewMarina($marinaName, $marinaBoss, $docks)
    {
        try {
            DB::insert('tbl_marinas', array(
                'marinaName' => $marinaName,
                'marinaBoss' => $marinaBoss,
                'docks' => $docks
            ));
        } catch (MeekroDBException $exception) {
            $string = "Kan ikke opprette havn. Hvis problemet vedvarer, vennligst kontakt systemadministrator.";
            sqlErrorDisplayer($string, false);
        }

    }

    public static function updateMarina($marinaId, $marinaName, $marinaBoss, $docks)
    {
        try {
            DB::update('tbl_marinas', array(
                'marinaName' => $marinaName,
                'marinaBoss' => $marinaBoss,
                'docks' => $docks,
            ), "marinaID=%i", $marinaId);
        } catch (MeekroDBException $exception) {
            sqlErrorDisplayer($exception, true);
        }
    }

    public static function removeMarina($marinaId)
    {
        try {
            DB::delete('tbl_marinas', "marinaID=%i", $marinaId);
        } catch (MeekroDBException $exception) {
            sqlErrorDisplayer($exception, true);
        }
    }

    public static function getMarinaBoss()
    {
        try {
            $marinaBossArray[] = [];
            $results = DB::queryRaw("SELECT * FROM tbl_user where role = 3");
            while ($obj = mysqli_fetch_object($results, User::class)) {
                $marinaBossArray[] = $obj;
            }
            return $marinaBossArray;
        } catch (MeekroDBException $exception) {
            sqlErrorDisplayer($exception, true);
        }
        return null;
    }
}