<?php
/**
 * Created by PhpStorm.
 * User.class: Peter
 * Date: 18.02.2019
 * Time: 22:30
 */

class Application_db
{

    /*
        0 = I Venteliste
        1 = Godkjent
        2 = Godkjent (framleie)

        3 = Framleie venteliste
        4 = Framleie godkjent
     */
    public static function getAllApplications()
    {
        $application[] = [];
        try {
            $mysqli_result = DB::queryRaw("SELECT * FROM tbl_dockingApplication");
            while ($obj = mysqli_fetch_object($mysqli_result, Application::class)) {
                $application[] = $obj;
            }
        } catch (MeekroDBException $exception) {
            sqlErrorDisplayer($exception, true);
        }
        return $application;
    }

    public static function getApplicationByID($applID) : Application
    {
        try {
            $mysqli_result = DB::queryRaw("select * from tbl_dockingApplication where applicationID=%i", $applID);
            if ($mysqli_result->num_rows == 0) {
                return new Application();
            }
            $appl = mysqli_fetch_object($mysqli_result, Application::class);
            if ($appl instanceof Application) {
                return $appl;
            }
        } catch (InvalidArgumentException $exception) {
            print ($exception->getMessage() . PHP_EOL);
        } catch (MeekroDBException $exception1) {
            sqlErrorDisplayer($exception1, true);
        }
        return null;
    }
    public static function getApplicationByBoatID($boatId) : Application
    {
        try {
            $mysqli_result = DB::queryRaw("select * from tbl_dockingApplication where boatID=%i", $boatId);
            if ($mysqli_result->num_rows == 0) {
                return new Application();
            }
            $appl = mysqli_fetch_object($mysqli_result, Application::class);
            if ($appl instanceof Application) {
                return $appl;
            }
        } catch (InvalidArgumentException $exception) {
            print ($exception->getMessage() . PHP_EOL);
        } catch (MeekroDBException $exception1) {
            sqlErrorDisplayer($exception1, true);
        }
        return null;
    }
	public static function getApplicationByDockingID($dockingID) : Application
	{
		try {
			$mysqli_result = DB::queryRaw("select * from tbl_dockingApplication where dockingID=%i", $dockingID);
			if ($mysqli_result->num_rows == 0) {
				return new Application();
			}
			$appl = mysqli_fetch_object($mysqli_result, Application::class);
			if ($appl instanceof Application) {
				return $appl;
			}
		} catch (InvalidArgumentException $exception) {
			print ($exception->getMessage() . PHP_EOL);
		} catch (MeekroDBException $exception1) {
			sqlErrorDisplayer($exception1, true);
		}
		return null;
	}

    public static function getAllApplicationsFromUser($userId)
    {
        try {
            $result = DB::query("SELECT applicationID, tbl_boat.boatID, description, status, date, tbl_boat.boatName
                            , tbl_boat.userId FROM `tbl_dockingApplication` INNER JOIN tbl_boat 
                            ON tbl_boat.boatID = tbl_dockingApplication.boatID WHERE tbl_boat.userId =%i ORDER BY applicationID DESC", $userId);
            for ($i = 0; $i < count($result); $i++) {
                if ($result[$i]['status'] == 0) {
                    $position = self::getApplicationPosition($result[$i]['boatID']);
                    $result[$i]["position"] = $position;
                }
            }
            return $result;
        } catch (MeekroDBException $exception) {
            sqlErrorDisplayer($exception, true);
        }
    }

    public static function getAllUnapprovedApplications()
    {
        $application[] = [];
        try {
            $mysqli_result = DB::query("SELECT * FROM tbl_dockingApplication WHERE status = 0");
        } catch (InvalidArgumentException $exception) {
            print ($exception->getMessage() . PHP_EOL);
        }
        return $mysqli_result;
    }

    public static function getApplicationPosition($boatId): int
    {
        try {
            $application = self::getAllUnapprovedApplications();
            $position = 1;
            foreach ($application as $key => $value) {
                if ($value['boatID'] == $boatId) {
                    $position = intval($position);
                    return $position;
                }
                $position++;
            }
            return 0;
        } catch (MeekroDBException $exception) {
            sqlErrorDisplayer($exception, true);
        }
    }

    public static function getAllBoatsFromUserWithoutAnApplicationOrDocking($userId)
    {
        try {
            $result = DB::query("SELECT tbl_boat.boatID, boatName, boatRegisterNumber FROM `tbl_boat` LEFT JOIN tbl_dockingApplication ON tbl_boat.boatID = tbl_dockingApplication.boatID WHERE tbl_dockingApplication.boatID IS NULL AND tbl_boat.userID=%i", $userId);
            return $result;
        } catch (MeekroDBException $exception) {
            sqlErrorDisplayer($exception, true);
        }
    }

    public static function getApplicationForBoat($boatId)
    {
        $result = DB::query("SELECT * FROM tbl_dockingApplication WHERE boatID=%i", $boatId);
        try {
            for ($i = 0; $i < count($result); $i++) {
                $result[$i]['position'] = self::getApplicationPosition($boatId);
            }
            return $result;
        } catch (MeekroDBException $exception) {
            sqlErrorDisplayer($exception, true);
        }
    }

    public static function newDockingApplication($boatId, $description)
    {
        try {
            $boat = Boat_db::getBoatByBoatId($boatId);
        } catch (MeekroDBException $exception) {
            $string = "Kan ikke søke om båtplass. Finner ikke båten. Feilmelding: \\n" . $exception->getMessage();
            sqlErrorDisplayer($string, false);
        }
        if ($boat instanceof Boat) {
            $id = $boat->getBoatId();
            $date = date('Y-m-d');
            try {
                DB::insert('tbl_dockingApplication', array(
                    'boatID' => $id,
                    'description' => $description,
                    'status' => 0,
                    'date' => (string)$date
                ));
            } catch (MeekroDBException $exception) {
                $string = "Kan ikke legge inn søknad. Ukjent feil. Feilmelding: \\n" . $exception->getMessage();
                sqlErrorDisplayer($string, false);
            }
        }
    }

    public static function newFramleieApplication($dockingID)
    {
        $docking = null;
        try {
            $docking = Docking_db::getDockingByID($dockingID);
        } catch (MeekroDBException $exception) {
            $string = "Kan ikke søke om framleie av båtplass. Finner ikke båtplassen. Feilmelding: \\n" . $exception->getMessage();
            sqlErrorDisplayer($string, false);
        }
        $id = $docking->getDockingID();
        $date = date('Y-m-d');
        try {
            DB::insert('tbl_dockingApplication', array(
                'dockingID' => $id,
                'description' => "Søknad om framleie",
                'status' => 3,
                'date' => (string)$date
            ));
        } catch (MeekroDBException $exception) {
            $string = "Kan ikke legge inn søknad. Ukjent feil. Feilmelding: \\n" . $exception->getMessage();
            sqlErrorDisplayer($string, false);
        }
    }

    public static function newApprovedDockingApplication($boatId, $description)
    {
        try {
            $boat = Boat_db::getBoatByBoatId($boatId);
        } catch (MeekroDBException $exception) {
            $string = "Kan ikke søke om båtplass. Finner ikke båten. Feilmelding: \\n" . $exception->getMessage();
            sqlErrorDisplayer($string, false);
        }
        if ($boat instanceof Boat) {
            $id = $boat->getBoatId();
            $date = date('Y-m-d');
            try {
                DB::insert('tbl_dockingApplication', array(
                    'boatID' => $id,
                    'description' => $description,
                    'status' => 1,
                    'date' => (string)$date
                ));
            } catch (MeekroDBException $exception) {
                $string = "Kan ikke legge inn søknad. Ukjent feil. Feilmelding: \\n" . $exception->getMessage();
                sqlErrorDisplayer($string, false);
            }
        }
    }

    public
    static function setStatus($applicationId, $status)
    {
        try {
            DB::update('tbl_dockingApplication', array(
                'status' => $status,
            ), "applicationID=%i", $applicationId);
        } catch (MeekroDBException $exception) {
            sqlErrorDisplayer($exception, true);
        }
    }

    public
    static function deleteApplication($applicationID)
    {
        try {
            DB::delete('tbl_dockingApplication', "applicationID=%i", $applicationID);
        } catch (MeekroDBException $exception) {
            sqlErrorDisplayer($exception->getMessage(), true);
        }
    }

    public
    static function deleteApplicationWithBoat($boat)
    {
        try {
            DB::delete('tbl_dockingApplication', "boatID=%i", $boat);
        } catch (MeekroDBException $exception) {
            sqlErrorDisplayer($exception->getMessage(), true);
        }
    }
}