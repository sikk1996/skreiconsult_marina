<?php
/**
 * Created by PhpStorm.
 * User.class: Peter
 * Date: 18.02.2019
 * Time: 22:30
 */


class Boat_db
{

    public static function createNewBoatForUser($uId, $boatName, $registerNumber, $manufacturer, $model, $length, $depth, $width, $type)
	{
        try{
            DB::insert('tbl_boat', array(
                'userId' => $uId,
                'boatName' => $boatName,
                'boatRegisterNumber' => $registerNumber,
                'manufacturer' => $manufacturer,
                'model' => $model,
                'length' => $length,
                'depth' => $depth,
                'width' => $width,
                'type' => $type	));


        }catch (MeekroDBException $exception)
        {
            sqlErrorDisplayer($exception, true);
        }

	}

    public static function updateBoat($boatId, $boatName, $registerNumber, $manufacturer, $model, $length, $depth, $width, $type)
    {
        try {
            if ($boatId == null) {
                return;
            }
            if ($boatName == null) {
                $result = DB::query("SELECT boatName FROM tbl_boat WHERE boatID=%i", $boatId);
                $boatName = $result[0]['boatName'];
            }
            if ($manufacturer == null) {
                $result = DB::query("SELECT manufacturer FROM tbl_boat WHERE boatID=%i", $boatId);
                $manufacturer = $result[0]['manufacturer'];
            }
            if ($registerNumber == null) {
                $result = DB::query("SELECT boatRegisterNumber FROM tbl_boat WHERE boatID=%i", $boatId);
                $registerNumber = $result[0]['boatRegisterNumber'];
            }
            if ($model == null) {
                $result = DB::query("SELECT model FROM tbl_boat WHERE boatID=%i", $boatId);
                $model = $result[0]['model'];
            }
            if ($length == null) {
                $result = DB::query("SELECT length FROM tbl_boat WHERE boatID=%i", $boatId);
                $length = $result[0]['length'];
            }
            if ($depth == null) {
                $result = DB::query("SELECT depth FROM tbl_boat WHERE boatID=%i", $boatId);
                $depth = $result[0]['depth'];
            }
            if ($width == null) {
                $result = DB::query("SELECT width FROM tbl_boat WHERE boatID=%i", $boatId);
                $width = $result[0]['width'];
            }
            if ($type == null) {
                $result = DB::query("SELECT type FROM tbl_boat WHERE boatID=%i", $boatId);
                $type = $result[0]['type'];

            }
            DB::update('tbl_boat', array(
                'boatID' => $boatId,
                'boatName' => $boatName,
                'manufacturer' => $manufacturer,
                'boatRegisterNumber' => $registerNumber,
                'model' => $model,
                'length' => $length,
                'depth' => $depth,
                'width' => $width,
                'type' => $type,

            ), "boatID=%i", $boatId);

        } catch (MeekroDBException $exception) {
            sqlErrorDisplayer($exception, true);
        }

    }

	public static function removeBoatFromUser($boatId)
	{
		DB::delete('tbl_boat', "boatID=%i", $boatId);
	}

	public static function getBoatByBoatId($boatId) :Boat
    {
        try {
            $mysqli_result = DB::queryRaw("SELECT * FROM tbl_boat WHERE boatID=%i", $boatId);
            if ($mysqli_result->num_rows == 0) {
                return new Boat();
            }
            $boat = mysqli_fetch_object($mysqli_result, Boat::class);
            if ($boat instanceof Boat) {
                return $boat;
            }
        } catch (InvalidArgumentException $exception) {
            print ($exception->getMessage() . PHP_EOL);
        } catch (MeekroDBException $exception1) {
            sqlErrorDisplayer($exception1, true);
        }
        return null;
    }


	public static function getAllBoats()
	{
		try{
			$mysqli_result = DB::queryRaw("SELECT * FROM tbl_boat");
			$boatArray[] = array();

			if($mysqli_result)
			{
				while($obj = mysqli_fetch_object($mysqli_result, Boat::class))
				{
					$boatArray[] = $obj;
				}
			}
            return $boatArray;
		}catch (MeekroDBException $exception)
		{
            sqlErrorDisplayer($exception, true);
		}

	}
	public static function getAllBoatsWithOwner()
    {
        try {
            $result = DB::query("SELECT * FROM `tbl_boat` INNER JOIN tbl_user ON tbl_user.userID = tbl_boat.userId ORDER BY tbl_user.lastname");
            return $result;
        } catch (MeekroDBException $exception) {
            sqlErrorDisplayer($exception, true);
        }
    }
	public static function getAllBoatsInDock($dockId)
	{
        try {
            $boatArray[] = array();
            $results = DB::query("SELECT * FROM tbl_boat INNER JOIN tbl_dockings ON tbl_boat.boatID = tbl_dockings.boat WHERE tbl_dockings.dockID=%i", $dockId);
            while ($obj = $results->fetch_object('Boat')) {
                array_push($boatArray, $obj);
            }
            return $boatArray;
        } catch (MeekroDBException $exception) {
            sqlErrorDisplayer($exception, true);
        }
	}
    public static function getAllOfUserBoats($userId)
    {
        $boats[] = [];
        try {
            $mysqli_result = DB::queryRaw("SELECT * FROM tbl_boat WHERE userId=%i", $userId);
            while ($obj = mysqli_fetch_object($mysqli_result, Boat::class))
            {
                $boats[] = $obj;
            }
        }catch (MeekroDBException $exception)
        {
            sqlErrorDisplayer($exception, true);
        }
        return $boats;
    }

	public static function getAllBoatsInMarina($marinaId)
	{
        try {
            $boatArray[] = array();
            $results = DB::query("SELECT * FROM tbl_boat INNER JOIN tbl_dockings ON tbl_boats.boatID = tbl_dockings.boat 
			INNER JOIN tbl_docks ON tbl_dockings.dock = tbl_docks.dockID INNER JOIN tbl_marinas ON tbl_docks.marina = tbl_marinas.marinaID
			  WHERE tbl_dockings.marina=%i", $marinaId);
            while ($obj = $results->fetch_object($results, Boat::class)) {
                array_push($boatArray, $obj);
            }
            return $boatArray;
        } catch (MeekroDBException $exception) {
            sqlErrorDisplayer($exception, true);
        }
	}

	public static function getRegNumberFromRegisteredBoats($regNumber) {
        try {
            $result = DB::query("SELECT boatRegisterNumber FROM tbl_boat WHERE boatRegisterNumber=%s", $regNumber);
            if (sizeof($result) == 0)
                return true;
            return false;
        } catch (MeekroDBException $exception) {
            sqlErrorDisplayer($exception, true);
        }
    }
}