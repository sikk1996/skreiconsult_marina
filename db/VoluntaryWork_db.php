<?php


class VoluntaryWork_db
{

    public static function newVoluntaryWork($userID, $hoursRegistered, $description, $date)
    {
        try {
            DB::insert('tbl_voluntaryWork', array(
                'userID' => $userID,
                'hoursRegistered' => $hoursRegistered,
                'description' => $description,
                'date' => $date
            ));
        } catch (MeekroDBException $exception) {
            sqlErrorDisplayer($exception, true);
        }
    }

    public static function getLastTenVoluntaryWork()
    {
        try {
            $mysqli_result = DB::query("SELECT workID, tbl_voluntaryWork.userID AS userID, hoursRegistered, description, date, email,tbl_user.firstname AS firstname,tbl_user.lastname AS lastname FROM tbl_voluntaryWork INNER JOIN tbl_user ON tbl_voluntaryWork.userID=tbl_user.userID ORDER BY date DESC LIMIT 10");
        } catch (InvalidArgumentException $exception) {
            print ($exception->getMessage() . PHP_EOL);
        } catch (MeekroDBException $exception) {
            sqlErrorDisplayer($exception, true);
        }
        return $mysqli_result;
    }

    public static function getVoluntaryWorkForUser($userId)
    {
        try {
            $result = DB::query("SELECT * FROM tbl_voluntaryWork WHERE userID=%i ORDER BY date asc", $userId);
            return $result;
        } catch (MeekroDBException $exception) {
            sqlErrorDisplayer($exception, true);
        }
        return null;
    }

    public static function getAllVoluntaryWorkOrderedByHours()
    {
        $voluntaryWork[] = [];
        try {
            $mysqli_result = DB::query("SELECT DISTINCT tbl_voluntaryWork.userID, firstname, lastname, email, tbl_voluntaryWork.date, tbl_voluntaryWork.hoursRegistered
											, tbl_voluntaryWork.description FROM tbl_voluntaryWork 
											INNER JOIN tbl_user ON tbl_voluntaryWork.userID=tbl_user.userID");
            $index = 0;

            return $mysqli_result;
        } catch (InvalidArgumentException $exception) {
            print ($exception->getMessage() . PHP_EOL);
        } catch (MeekroDBException $exception) {
            sqlErrorDisplayer($exception, true);
        }
        return null;
    }

    public static function getTotalHoursFromUser($userId)
    {
        $hours = 0;
        try {
            $mysqli_result = DB::query("SELECT hoursRegistered FROM tbl_voluntaryWork WHERE userID=%i", $userId);
            foreach ($mysqli_result as $obj) {
                $hours += $obj['hoursRegistered'];
            }
        } catch (InvalidArgumentException $exception) {
            print ($exception->getMessage() . PHP_EOL);
        } catch (MeekroDBException $exception) {
            sqlErrorDisplayer($exception, true);
        }
        return $hours;
    }
}