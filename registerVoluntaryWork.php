<?php
require_once "helper.php";
$feedbackValidated = true;
$feedbackTitle = "";
$feedbackText = "";

if (isset($_SESSION['user_role'])){

    //Hvis admin og satt ?userId
    if (isset($_GET['userId']) && $_SESSION['user_role']>=3){
        $user = User_db::getUserByUserId($_GET['userId']);
    } else { //Hvis ikke -> bruk egen user
        $user = User_db::getUserByUserId($_SESSION['user_id']);
    }

    if(isset($_POST['confirm'])) {
        $feedbackValidated=validateInputFields();
        if ($feedbackValidated) {
            newVoluntaryWork($user);
        }
    }

    echo $twig->render('templates/registerVoluntaryWork.twig',array('feedbackValidated'=>$feedbackValidated, 'feedbackTitle'=>$feedbackTitle, 'feedbackText'=>$feedbackText));
} else { //Hvis ikke innlogget gtfo
    header('Location: login.php?alertType="danger"&alertText="Du må logge inn for å registrere dugnadsarbeid"');
}

function newVoluntaryWork($user){
    if($user instanceof User){
        $hoursRegistered = $_POST['hoursInput'];
        $description = $_POST['descriptionInput'];
        $date = $_POST['dateInput'];
        VoluntaryWork_db::newVoluntaryWork($user->getUserID(), $hoursRegistered, $description, $date);
        header('Location: profile.php?userId='.$user->getUserID());

    }
}
function validateInputFields()
{
    global $feedbackTitle;
    global $feedbackText;

    if (!empty($_POST['descriptionInput'])) {
        if (!helperFunctions::validateString(array($_POST['descriptionInput']))) {
            $feedbackTitle = "Feilmelding";
            $feedbackText= "Beskrivelsen inneholder ugyldige tegn";
            return false;
        }
    }
    if (!empty($_POST['hoursInput'])) {
        if (!helperFunctions::validateInteger($_POST['hoursInput'])) {
            $feedbackTitle = "Feilmelding";
            $feedbackText= "Timer kan bare være tall";
            return false;
        }
    }
    return true;
}