<?php

require_once "helper.php";
$feedbackValidated = true;
$feedbackTitle = "";
$feedbackText = "";

///is logged in
if (isset($_SESSION['user_role'])) {
	if ($_SESSION['user_role'] == 0) {
		echo $twig->render('templates/unverifiedUser.twig', array());
		exit();
	}

	//Hvis admin og satt ?userId
	if (isset($_GET['userId']) && $_SESSION['user_role'] >= 3) {
		$user = User_db::getUserByUserId($_GET['userId']);
	} else { //Hvis ikke -> bruk egen user
		$user = User_db::getUserByUserId($_SESSION['user_id']);
	}

	if (isset($_POST['confirm'])) {
		$feedbackValidated = validateInputFields();
		if ($feedbackValidated) {
			createNewBoatForUser($user->getUserID());
			header('Location: profile.php');
		}
	}
	echo $twig->render('templates/registerBoat.twig', array('feedbackValidated' => $feedbackValidated, 'feedbackTitle' => $feedbackTitle, 'feedbackText' => $feedbackText));

} else { //Hvis ikke innlogget gtfo
	header('Location: login.php?alertType="danger"&alertText="Du må logge inn for å legge inn båt"');
}

function createNewBoatForUser($userId) {
	$boatName = $_POST['boatNameInput'];
	$registerNumber = $_POST['boatRegisterNumberInput'];
	$manufacturer = $_POST['manufactorInput'];
	$model = $_POST['modelNameInput'];
	$length = $_POST['lengthInput'];
	$depth = $_POST['depthInput'];
	$width = $_POST['widthInput'];
	$type = $_POST['typeInput'];
	Boat_db::createNewBoatForUser($userId, $boatName, $registerNumber, $manufacturer, $model, $length, $depth, $width, $type);
}

function validateInputFields() {
	global $feedbackTitle;
	global $feedbackText;
	if (!empty($_POST['boatRegisterNumberInput'])) {
		if (!Boat_db::getRegNumberFromRegisteredBoats($_POST['boatRegisterNumberInput'])) {
			$feedbackTitle = "Feilmelding";
			$feedbackText = "Registreringsnummeret finnes allerede i vår database";
			return false;
		}
	} else $_POST['boatRegisterNumberInput'] = null;

	if ((empty($_POST['lengthInput'])) || (empty($_POST['depthInput'])) || (empty($_POST['widthInput']))) {
		$feedbackTitle = "Feilmelding";
		$feedbackText = "Felt  med * må fylles ut!";
		return false;
	}

	if (!empty($_POST['boatNameInput'])) {
		if (!helperFunctions::validateString(array($_POST['boatNameInput']))) {
			$feedbackTitle = "Feilmelding";
			$feedbackText = "Båtnavn inneholder ugyldige tegn";
			return false;
		}
	}

	if (!empty($_POST['boatRegisterNumberInput'])) {
		if (!helperFunctions::validateString(array($_POST['boatRegisterNumberInput']))) {
			$feedbackTitle = "Feilmelding";
			$feedbackText = "Båtregistreringsnummer inneholet ugyldige tegn";
			return false;
		}
	}

	if (!empty($_POST['manufactorInput'])) {
		if (!helperFunctions::validateString(array($_POST['manufactorInput']))) {
			$feedbackTitle = "Feilmelding";
			$feedbackText = "Båtmerke inneholder ugyldige tegn";
			return false;
		}
	}

	if (!empty($_POST['modelNameInput'])) {
		if (!helperFunctions::validateString(array($_POST['modelNameInput']))) {
			$feedbackTitle = "Feilmelding";
			$feedbackText = "Båtmodell inneholder ugyldige tegn";
			return false;
		}
	}

	if (!empty($_POST['typeInput'])) {
		if (!helperFunctions::validateString(array($_POST['typeInput']))) {
			$feedbackTitle = "Feilmelding";
			$feedbackText = "Båttype inneholder ugyldige tegn";
			return false;
		}
	}
	return true;
}