<?php

require_once "helper.php";
if (isset($_SESSION['user_role']) && $_SESSION['user_role'] >= 2) {

	//Sjekk om bryggesjef eller havnsjef, og bygg array av users på tilgangen.
	if ($_SESSION['user_role'] == 2) {
		$docks = Dock_db::getAllDocksWhereDockBoss($_SESSION['user_id']);
		$users = array();
		foreach ($docks as $dock) {
			if ($dock instanceof Dock) {
				$users = array_merge($users, User_db::getAllUsersByDock($dock->getDockID()));
			}
		}
	} else {
		$users = User_db::getAllUsers();
	}
	foreach ($users as $user) {
		if ($user instanceof User) {
			$hoursWorked = VoluntaryWork_db::getTotalHoursFromUser($user->getUserID());
			$user->setHoursWorked($hoursWorked);
		}
	}
	//Role change
	if (isset($_POST['btnPickNewRole']) && isset($_POST['userID']) && $_SESSION['user_role'] >= 3) {
		$newRole = $_POST['btnPickNewRole'];
		$userID = $_POST['userID'];
		$pickedUser = User_db::getUserByUserId($userID);
		$oldRole = $pickedUser->getRole();

		//Must check if bryggesjef --> remove from old docks
		if ($oldRole == 2) {
			//Remove bryggesjef from Dock
			$docks = Dock_db::getAllDocksWhereDockBoss($_POST['userID']);
			foreach ($docks as $dock) {
				if ($dock instanceof Dock) {
					Dock_db::updateDockBoss($dock->getDockID(), null);
				}
			}
		}
		//Assign to new docks
		if ($newRole == 2 && isset($_POST['docks'])) {
			//Assign bryggesjef to Dock
			$dockIDs = $_POST['docks'];
			foreach ($dockIDs as $dockID) {
				Dock_db::updateDockBoss($dockID, $userID);
			}
		}

		//Change user role (and update session variable if it is your own)
		User_db::setUserRights($_POST['userID'], $newRole);
		if ($_POST['userID'] == $_SESSION['user_id'])
			$_SESSION['user_role'] = $newRole;
		header('Location: users.php');
	}

	//Filtrering av roller
	$role = -1;
	if (isset($_GET['role'])) {
		$role = $_GET['role'];
	}

	//Inkall til dugnad
	if (isset($_POST['sendEmails'])) {
		if (isset($_POST['hiddenEmails'])) {
			$emails = $_POST['hiddenEmails'];
			$emailStrings = explode(",", $emails);
			$filteredArray = array_filter($emailStrings);
			foreach ($filteredArray as $email) {
				$email = trim($email);
				sendMail($email, $_POST['emailTitle'], $_POST['emailContent']);
			}
		}
	}

	if (isset($_GET['alertType']) && isset($_GET['alertText'])) {
		echo $twig->render('templates/users.twig', array('users' => $users, 'alertType' => $_GET['alertType'], 'alertText' => $_GET['alertText']));
	} else {
		echo $twig->render('templates/users.twig', array('users' => $users, 'filterRole' => $role, 'DocksWithoutBoss' => Dock_db::getAllDocksWithoutDockBoss()));
	}

} else {
	header('Location: index.php');
}

function validateInputFields() {
	global $modifyUserFeedbackBad;
	if (!helperFunctions::validateString(array($_POST['emailTitle'], $_POST['emailContent']))) {
		$modifyUserFeedbackBad = "Tittel eller innhold inneholder ugyldige tegn.";
		return false;
	}
	return true;
}