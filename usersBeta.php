<?php
require_once "helper.php";

if (isBryggesjefOrHigher()) {

	//Check for Ajax Calls
	if(ajaxRequest()){
		$ajax = $_POST['ajax'];
		header("Content-Type: application/json; charset=UTF-8");
		if ($ajax==1){
			echo User_db::getAllUsersJSON();
			exit;
		}
	}
	//Ajax end

	//Load page
	if (isBryggesjef()){


	}
	elseif (isHavnesjef()){

	}

	//Inkall til dugnad
	if(isset($_POST['sendEmails'])) {
		if(isset($_POST['hiddenEmails'])) {
			$emails = $_POST['hiddenEmails'];
			$emailStrings = explode(",", $emails);
			$filteredArray = array_filter($emailStrings);

			foreach ($filteredArray as $email)
			{
				$email = trim($email);
				sendMail($email, $_POST['emailTitle'], $_POST['emailContent']);
			}
		}
	}





} else {
	header('Location: index.php');
}

echo $twig->render('templates/usersBeta.twig', array());



function ajaxRequest(){
	return isset($_POST['ajax']);
}

function isLoggedIn(){
	return isset($_SESSION['user_role']);
}
function isBryggesjef(){
	return isset($_SESSION['user_role']) & $_SESSION['user_role']==2;
}
function isHavnesjef(){
	return isset($_SESSION['user_role']) & $_SESSION['user_role']==3;
}
function isBryggesjefOrHigher(){
	return isset($_SESSION['user_role']) & $_SESSION['user_role']>=2;
}



