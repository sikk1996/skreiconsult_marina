<?php
/**
 * Created by PhpRain.
 * User: UserName
 * Date: 25.01.2016
 * Time: 19:23
 */

require_once "helper.php";

$feedbackGood = "";

if (isset($_POST['buttonDeleteUserInfo']) && isset($_SESSION['user_id']) && !isset($_POST['buttonExportUserInfo'])) {
    if ($_POST['buttonDeleteUserInfo'] == $_SESSION['user_id']) {
        $tempBoat = Boat_db::getBoatByBoatId($_POST['buttonDeleteUserInfo']);
        if ($tempBoat instanceof Boat) {
            if ($tempBoat->getUserId() != $_SESSION['user_id']) {
                exit();
            } else {
                deleteUserInfo((int)$_POST['buttonDeleteUserInfo']);
            }
        }
    } else {
        header('Location: index.php');
    };
} elseif (isset($_POST['buttonExportUserInfo']) && isset($_SESSION['user_id'])) {
    if ($_POST['buttonExportUserInfo'] == $_SESSION['user_id']) {
        $userID = (int)$_POST['buttonExportUserInfo'];
        exportUserInfo($userID);
        header('Location: profile.php');
    } else {
        header('Location: index.php');
    }
} else {
    header('Location: index.php');
}

function exportUserInfo(int $userId)
{
    $stringInfoStart = "Du har bed om å få tilsendt informasjon om du bruker fra Skrei Consult. <br> Her er det vi har lagret om deg.<br><br>";
    $stringInfoEnd = "<br><br><br>Mvh<br>Skrei Marina";
    $user = User_db::getUserByUserId($userId);
    $userToString = $user->exportToString() . "<br>";
    $boats = Boat_db::getAllOfUserBoats($userId);
    $boatsToString = "";
    foreach ($boats as $boat) {
        if ($boat instanceof Boat) {
            $boatsToString = $boatsToString . "<br>" . $boat->exportToString();
        }
    }
    $responsText = $stringInfoStart . $userToString . $boatsToString . $stringInfoEnd;
    sendMail($user->getEmail(), "Eksport av registrert brukerinformajon", $responsText);
}

function deleteUserInfo(int $userId)
{
    User_db::deleteUser($userId);
    header('Location: ?logout');
}