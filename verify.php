<?php
require_once 'helper.php';

if (isset($_GET['keyValue']) && isset($_GET['id']) ) {
    $emailVerificationKey = $_GET['keyValue'];
    $id = $_GET['id'];
    if (User_db::getVerificationKeyByUserId($id) == $emailVerificationKey){
        User_db::setUserRights($id,1);
        User_db::removeVerificationKeyByUserId($id);
        session_destroy();
        header('Location: index.php?alertType=alert-success&alertText=Kontoen din er verifisert!');
    } else {
        header('Location: index.php?alertType=alert-danger&alertText=Feil i verifisering!');
    }
} else {
    header('Location: index.php?alertType=alert-danger&alertText=Du forsøkte å nå et lukket område! Ditt forsøk er registrert.');
}

//https://109.189.39.179:63636/verify.php?id=56&keyValue=ngq76hREt0scQxgb1MLCg8WTj