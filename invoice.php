<?php
require_once "helper.php";
if (isset($_SESSION['user_role']) && $_SESSION['user_role'] >= 3) {
	$invoices = User_db::getAllInvoices();
	$users = User_db::getAllVerifiedUsers();
	$settings = Settings_db::getSettings();
	if (isset($_POST['saveDate'])) {
		$datePaid = $_POST['dateInput'];
		$invoiceID = $_POST['saveDate'];
		User_db::setDatePaid($invoiceID, $datePaid);
		header('Location: invoice.php');
	}

	//Role change
	if (isset($_POST['btnPickNewRole']) && isset($_POST['userID']) && $_SESSION['user_role'] >= 3) {
		$newRole = $_POST['btnPickNewRole'];
		$userID = $_POST['userID'];
		$pickedUser = User_db::getUserByUserId($userID);
		$oldRole = $pickedUser->getRole();
	}
	if (isset($_POST['exportInvoice']) && $_SESSION['user_role'] >= 3) {
		$exportString = "<h3>Ubetale fakturaer</h3><br><br>";
		$count = 0;
		foreach ($invoices as $invoice) {
			if ($invoice['datePaid'] === null) {
				$count++;
				//Legger til faktura informajson.
				$exportString .= "Faktura: " . $invoice['invoiceID'] . ", Forfall: " . $invoice['date'] . ", " . $invoice['lastname'] . " " . $invoice['firstname'] . ", Tlf: " . $invoice['phone'] . "<br>";
			}
		}
		if ($count === 0) {
			$exportString .= "<br>Ingen ubetalte fakturaer";
		} elseif ($count === 1) {
			$exportString .= "<br>Totalt 1 ubetalt faktura";
		} elseif ($count > 1) {
			$exportString .= "<br>Totalt " . $count . " ubetalte fakturaer";
		} else {
			$exportString .= "<br>Kan ikke hente totalt antall ubetalte fakturaer.";
		}
		sendMail(User_db::getUserByUserId($_SESSION['user_id'])->getEmail(), "Eksport av ubetalte faktura", $exportString);
	}

	//Filtrering av betaltStatus
	$statusPaid = -1;
	if (isset($_GET['statusPaid'])) {
		$statusPaid = $_GET['statusPaid'];
	}
	if (isset($_POST['addInvoice'])) {
		$amount = $_POST['paymentAmount'];
		$id = $_POST['selectedUser'];
		User_db::createInvoice($id, $amount);
		header('Location: invoice.php');
	}

	//render page
	echo $twig->render('templates/invoice.twig', array('invoices' => $invoices, 'filterStatusPaid' => $statusPaid, 'users' => $users, 'settings' => $settings));

} else {
	header('Location: index.php');
}