<?php
require_once "helper.php";
$feedbackValidated = true;
$forgotFeedback = "";

if(isset($_POST['forgotBtn'])) {
    $email = $_POST['emailInput'];
    if(!helperFunctions::validateEmail($_POST['emailInput']))
    {
        $feedbackText = "Epost adresse er ikke gyldig";
        $feedbackValidated=false;
    }


    if($feedbackValidated == true)
    {
        $user = null;
        $password = helperFunctions::randomPassword(12);
        $passwordHash = helperFunctions::encryptPassword($password);
        $user = User_db::getUserByEmail($email);

        if ($user instanceof User){
            User_db::setUserPassword($user->getUserID(),$passwordHash);
            sendMail($email,"Nytt passord hos SkreiMarina","Hei, du har bedt om nytt passord hos Skrei Marina. Ditt nye passord er '".$password."'");
            $forgotFeedback = "Nytt passord er sendt til eposten";
        } else {
            $forgotFeedback = "Bruker eksisterer ikke";
        }

    }
}


echo $twig->render('templates/forgotPassword.twig',array('forgotFeedback'=>$forgotFeedback));?>