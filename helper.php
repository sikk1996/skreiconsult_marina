<?php
/**
 * Created by PhpStorm.
 * User.class: Peter
 * Date: 18.02.2019
 * Time: 22:32
 */

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

setlocale(LC_TIME, "norwegian");
date_default_timezone_set("Europe/Oslo");
if (!isset($_SESSION)) {
    session_start();
}

require_once 'vendor/autoload.php';

require_once 'db/DB_Setup.php';
require_once 'helperFunctions.php';

require_once 'classes/Application.class.php';
require_once 'classes/Boat.class.php';
require_once 'classes/Dock.class.php';
require_once 'classes/Docking.class.php';
require_once 'classes/Marina.class.php';
require_once 'classes/User.class.php';
require_once 'classes/UserAddress.class.php';
require_once 'classes/VoluntaryWork.class.php';
require_once 'classes/Framleie.class.php';

require_once 'db/Application_db.php';
require_once 'db/DockingAssigner.php';
require_once 'db/Boat_db.php';
require_once 'db/Dock_db.php';
require_once 'db/Docking_db.php';
require_once 'db/Marina_db.php';
require_once 'db/User_db.php';
require_once 'db/UserAddress_db.php';
require_once 'db/VoluntaryWork_db.php';
require_once 'db/Settings_db.php';


//twig
$loader = new Twig_Loader_Filesystem('/');
$twig = new Twig_Environment($loader);
$twig->addGlobal('session', $_SESSION);
$settings = Settings_db::getSettings();
$twig->addGlobal('marinaName', $settings['marinaName']);

//Postcodes
if (isset($_POST['postcode'])) {
    echo User_db::getPostalCity($_POST['postcode']);
}

//UTLOGGING
if (isset($_GET['logout'])) {
    session_destroy();
    header("Location: index.php");
}

function sendMail($email, $subject, $text)
{
    // password: yhaja4F9Xdx7t4K
    // https://github.com/PHPMailer/PHPMailer
    $mail = new PHPMailer(true);                    // Passing `true` enables exceptions
    try {
        //Server settings
        $mail->SMTPDebug = 0;                                 // Enable verbose debug output 2!!!!
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'send.one.com';                         // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'skreiconsult@schjem.net';          // SMTP username
        $mail->Password = 'yhaja4F9Xdx7t4K';                  // SMTP password
        $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 465;                                    // TCP port to connect to
        $mail->CharSet = 'UTF-8';

        //Recipients
        $mail->setFrom('skreiconsult@schjem.net', 'Skrei Marina');
        $mail->addAddress($email);     // Add a recipient
        //$mail->addAddress('ellen@example.com','Joe User');               // Name is optional
        $mail->addReplyTo('skreiconsult@schjem.net', 'Kundesupport Skrei Consult');
        //$mail->addCC('cc@example.com');
        //$mail->addBCC('bcc@example.com');

        //Attachments
        //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
        //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

        //Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject ='=?utf-8?B?'.base64_encode($subject).'?=';
        $mail->Body = '<meta content="text/html;charset=utf-8" />' . $text;//'This is the HTML message body <b>in bold!</b>';
        //$mail->Body = "<meta charset='UTF-8'>" . $text;//'This is the HTML message body <b>in bold!</b>';
        $mail->AltBody = $text;//'This is the body in plain text for non-HTML mail clients';
        $mail->send();
        //echo 'Message has been sent';
    } catch (Exception $e) {
        //echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
    }
}

function sendMailRegisteredUser($email)
{
    sendMail($email, "test", "testing");
}

function sqlErrorDisplayer($message, $default)
{
    if ($default) {
        $message="Ukjent feil har oppstått. Beklager.\\nHvis problemet vedvarer, kontakt systemadministrator.\\n\\n".$message;
    }
    echo '<script type="text/javascript">alert("'.$message.'")</script>';

}
