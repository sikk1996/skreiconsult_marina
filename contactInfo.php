<?php
/**
 * Created by PhpStorm.
 * User: lenetangstad
 * Date: 2019-04-08
 * Time: 11:51
 */

require_once "helper.php";

if (isset($_SESSION['user_role'])) {

    if ($_SESSION['user_role'] == 0){
        echo $twig->render('templates/unverifiedUser.twig', array());
        exit();
    }
    $docks = Dock_db::getAllDocks();
    $dockBosses = Dock_db::getAllDockBosses();
    $marinaBosses = Marina_db::getMarinaBoss();

    echo $twig->render('templates/contactInfo.twig', array('docks' => $docks, 'dockBosses' => $dockBosses, 'marinaBosses' => $marinaBosses));
}