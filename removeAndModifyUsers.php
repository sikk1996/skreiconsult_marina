<?php
/**
 * Created by PhpStorm.
 * User: Mermaid
 * Date: 19/03/2019
 * Time: 20:25
 */
require_once "helper.php";
$feedbackBad = "";
$feedbackGood = "";
//Sjekker om Knapp fra Bruker side er trykked og man er admin
if ($_SESSION['user_role'] >= 3) {
    if (isset($_POST['remove'])) {
        User_db::deleteUser($_POST['remove']);
        $feedbackGood = "Bruker fjernet";
        header('Location: users.php');
    }
    if (isset($_POST['editUser'])) {
        fetchAndDisplayUserDataForModification($_POST['editUser']);
    }

    if (isset($_POST['confirm']) && validateInputFields()) {
        modifyUserBasedOnPostReply();
        header('Location: users.php');
    }
} else {
    //Hindre uaturisert tilgang
    header('Location: index.php');
}


//Henter user data å setter det inn i modify slik at man får opp eksisterende informasjon.
function fetchAndDisplayUserDataForModification($id)
{
    global $twig;
    $user = User_db::getUserByUserId($id);
    $userAddress = UserAddress_db::getUserAddressByUserId($id);
    echo $twig->render('templates/adminModifyUser.twig', array('user' => $user, 'userAddress' => $userAddress));

}

//Tar Input fra skjema å setter det i database
function modifyUserBasedOnPostReply()
{

    global $modifyUserFeedbackGood;
    $userid = $_POST['userIdInput'];
    $firstname = $_POST['firstNameInput'];
    $lastname = $_POST['lastNameInput'];
    $email = $_POST['emailInput'];
    $phone1 = $_POST['phoneInputOne'];
    $phone2 = $_POST['phoneInputTwo'];
    $postCode = $_POST['postcodeInput'];
    $postAddress = $_POST['postaddressInput'];
    //$city = $_POST['city'];
    if ($phone2 == "")
        $phone2 = null;
    User_db::updateUser($userid, $email, null, null, null, $firstname, $lastname, $phone1, $phone2, null, $postCode, $postAddress, null);
    $modifyUserFeedbackGood = "Bruker modifisert.";
    header('Location: users.php');
}

//Standard verifisering
function validateInputFields()
{
    global $modifyUserFeedbackBad;
    if (!helperFunctions::validateEmail($_POST['emailInput'])) {
        $modifyUserFeedbackBad = "Epost er ikke gyldig";
        return false;
    }
    if (!helperFunctions::validateString(array($_POST['firstNameInput'], $_POST['lastNameInput']))) {
        $modifyUserFeedbackBad = "Fornavn eller etternavn inneholder ulovlige tegn";
        return false;
    }
    if (!helperFunctions::validateInteger($_POST['phoneInputOne']) && !helperFunctions::validatePhoneNumber($_POST['phoneInputOne'])) {
        $modifyUserFeedbackBad = "Telefon nummer skal være 8 tall";
        return false;
    }
    if (!empty($_POST['phoneInputTwo'])) {
        if (!helperFunctions::validateInteger($_POST['phoneInputTwo']) && !helperFunctions::validatePhoneNumber($_POST['phoneInputOne'])) {
            $modifyUserFeedbackBad = "Telefon nummer skal være 8 tall";
            return false;
        }
    }
    if (!helperFunctions::validateString(array($_POST['postaddressInput']))) {
        $modifyUserFeedbackBad = "Addressen inneholder et ugyldig tegn";
        return false;
    }
    if (!helperFunctions::validateInteger($_POST['postcodeInput'])) {
        $modifyUserFeedbackBad = "Postkoden inneholder et ugyldig tegn";
        return false;
    }
    return true;
}
