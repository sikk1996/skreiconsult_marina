<?php

require_once "helper.php";


$feedbackValidated = true;
$feedbackTitle = "";
$feedbackText = "";

if (isset($_POST['editBoat'])) {
    $boat = Boat_db::getBoatByBoatId($_POST['editBoat']);
}


if (isset($_SESSION['user_role'])) {

    if (isset($_POST['confirmChanges'])) {
        $boatID = $_POST['boatId'];
        $boat = Boat_db::getBoatByBoatId($boatID);
        $feedbackValidated = validateInputFieldsBoat();
        if ($feedbackValidated) {
            modifyBoatBasedOnPostReply($_POST['boatId']);
            header('Location: profile.php');
        }
    }
    echo $twig->render('templates/modifyBoat.twig', array('boat' => $boat, 'feedbackValidated' => $feedbackValidated, 'feedbackTitle' => $feedbackTitle, 'feedbackText' => $feedbackText));

} else {
    header('Location: index.php');
}

//Tar Input fra skjema å setter det i database
function modifyBoatBasedOnPostReply($boatID)
{

    global $modifyUserFeedbackGood;
    $boatName = $_POST['boatNameInput'];
    $registerNumber = $_POST['boatRegisterNumberInput'];
    $manufacturer = $_POST['manufactorInput'];
    $model = $_POST['modelNameInput'];
    $length = $_POST['lengthInput'];
    $depth = $_POST['depthInput'];
    $width = $_POST['widthInput'];
    $type = $_POST['typeInput'];
    Boat_db::updateBoat($boatID, $boatName, $registerNumber, $manufacturer, $model, $length, $depth, $width, $type);
    $modifyUserFeedbackGood = "Båten ble oppdatert.";
    header('Location: profile.php');
}

function validateInputFieldsBoat()
{
    global $feedbackTitle;
    global $feedbackText;

    if ((empty($_POST['lengthInput'])) || (empty($_POST['depthInput'])) || (empty($_POST['widthInput']))) {
        $feedbackTitle = "Feilmelding";
        $feedbackText = "Feltene  med * må fylles ut!";
        return false;
    }

    if (!empty($_POST['boatNameInput'])) {
        if (!helperFunctions::validateString(array($_POST['boatNameInput']))) {
            $feedbackTitle = "Feilmelding";
            $feedbackText = "Båtnavn inneholder ugyldig tegn";
            return false;
        }
    }
    if (!empty($_POST['boatRegisterNumberInput'])) {
        if (!helperFunctions::validateString(array($_POST['boatRegisterNumberInput']))) {
            $feedbackTitle = "Feilmelding";
            $feedbackText = "Båtregistreringsnummer inneholet ugyldig tegn";
            return false;
        }
    }
    if (!empty($_POST['manufactorInput'])) {
        if (!helperFunctions::validateString(array($_POST['manufactorInput']))) {
            $feedbackTitle = "Feilmelding";
            $feedbackText = "Båtmerke inneholder ugyldig tegn";
            return false;
        }
    }
    if (!empty($_POST['modelNameInput'])) {
        if (!helperFunctions::validateString(array($_POST['modelNameInput']))) {
            $feedbackTitle = "Feilmelding";
            $feedbackText = "Båtmodell inneholder ugyldig tegn";
            return false;
        }
    }
    if (!empty($_POST['typeInput'])) {
        if (!helperFunctions::validateString(array($_POST['typeInput']))) {
            $feedbackTitle = "Feilmelding";
            $feedbackText = "Båttype inneholder ugyldig tegn";
            return false;
        }
    }
    return true;
}
